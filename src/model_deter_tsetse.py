"""
Version 2.0
Hereafter is the implementation of the model of tsetse flies population dynamics.
Associated files are main_deter_tsetse.py and model_deter_tsetse.py

Contributors and contact:
-------------------------

    - Hélène Cecilia (helene.cecilia[at]oniris-nantes.fr)
    - Sandie Arnoux (sandie.arnoux[at]inra.fr)
    - Sébastien Picault (sebastien.picault[at]oniris-nantes.fr)
    - Pauline Ezanno (pauline.ezanno[at]inra.fr)

    BIOEPAR, INRA Oniris, Atlanpole La Chantrerie
    Nantes CS 44307 CEDEX, France



License:
--------
CeCILL-B
IDDN (Inter Deposit Digital Number) : coming soon

"""

import math
from   pathlib                    import Path
import numpy                      as np
from progress.bar import ChargingBar

from   tools_tsetse       import AutoEnum, diffuse2d_general


class Groups(AutoEnum):
    """Representation of distinct stages in tsetse
    flies, including males and sterile groups.

    """
    ### Each group is characterized by the following attributes:
    ### (gender, next development stages, egg_layer, moving, competitor, color)

    YOUNG_MALE = ('male', ['MALE'], False, True, True, 'deepskyblue')
    MALE = ('male', [], False, True, True, 'royalblue')
    STERILE_MALE = ('male', [], False, True, True, 'lightsteelblue')
    PUP = (None,
           [('TENERAL', lambda simul: 0.5*(1.-sterility_ratio(simul))),
            ('STERILE_TENERAL', lambda simul: 0.5*sterility_ratio(simul)),
            ('YOUNG_MALE', 0.5)],
           False, False, False, 'magenta')
    TENERAL = ('female', ['ADULT1'], True, True, True, 'red')
    STERILE_TENERAL = ('female', [], False, True, True, 'mistyrose')
    ADULT1 = ('female', ['ADULT2'], True, True, True, 'salmon')
    ADULT2 = ('female', ['ADULT3'], True, True, True, 'sandybrown')
    ADULT3 = ('female', ['ADULT4'], True, True, True, 'gold')
    ADULT4 = ('female', ['ADULT4'], True, True, True, 'yellow')

    def __init__(self, gender, devel, egg_layer, moving, competitor, color):
        self.gender = gender
        self.development = devel
        self.egg_layer = egg_layer
        self.moving = moving
        self.competitor = competitor
        self.color = color

    def __str__(self):
        return '{} ({}{})'.format(self.name,
                                  self.gender,
                                  ', laying eggs' if self.egg_layer else '')
    @property
    def index(self):
        return self.value - 1

    def next_stages(self, simul):
        if self.development:
            if len(self.development) == 1:
                return [(Groups[name], np.ones((simul.height, simul.width)))
                        for name in self.development]
            else:
                return [(Groups[name],
                         proportion(simul) if callable(proportion)\
                           else proportion*np.ones((simul.height, simul.width)))
                        for name, proportion in self.development
                       ]
        else:
            return []

    @classmethod
    def females(cls):
        """Return female-only stages."""
        return filter(lambda x: x.gender == 'female', cls)

    @classmethod
    def males(cls):
        """Return male-only stages."""
        return filter(lambda x: x.gender == 'male', cls)

    @classmethod
    def competitors(cls):
        """Return stages involved in competition for resources
        (counted in total population).

        """
        return filter(lambda x: x.competitor, cls)

    @classmethod
    def developing(cls):
        return filter(lambda x: x.development, cls)

    @classmethod
    def laying_eggs(cls):
        return filter(lambda x: x.egg_layer, cls)

    @classmethod
    def migrating(cls):
        return filter(lambda x: x.moving, cls)

    @classmethod
    def mortal(cls):
        return cls

    @classmethod
    def eggs(cls):
        return Groups.PUP



def sterility_ratio(simul):
    """Return the proportion of sterile males in the adult male
    population.

    """
    males = simul.current_pop[Groups.MALE].sum(axis=2)
    sterile_males = simul.current_pop[Groups.STERILE_MALE].sum(axis=2)
    total = males + sterile_males
    total[total == 0] = 1.
    return sterile_males / (total * simul.param.sterility_penality)


def devel_rate(stage, param):
    """Return the development rate associated with the specified
    stage.

    """
    if stage == Groups.PUP:
        def devel(temp):
            """Return the development rate associated with the specified
            environmental conditions (temperatures).

            """
            return param.dev_P * 1/(param.d1_P + (param.d2_P - param.d1_P)/(1 + np.exp((param.d3_P - temp) / param.d4_P)))
        return devel
    elif stage == Groups.YOUNG_MALE:
        def devel(temp):
            """Return the development rate associated with this
            group.

            """
            return np.ones(temp.shape)*1./6.
        return devel
    else:
        d1, d2 = (param.d1_T, param.d2_T) if stage == Groups.TENERAL else (param.d1_F, param.d2_F)
        correc = param.dev_T if stage == Groups.TENERAL else param.dev_F
        def devel(temp):
            """Return the development rate associated with the specified
            environmental conditions (temperatures).

            """
            return correc * ((temp - param.T_0)*d1 + d2)
        return devel


def mortality_rate_control(stage, param, mask):
    """Return the mortality rate associated with the specified
    stage.

    """
    if stage == Groups.PUP:
        def mortality(temp, cap, pop):
            """Return the mortality rate associated with the specified
            environmental conditions (temperature, capacity,
            population).

            """
            return np.exp(- param.mort_P * param.m_P), mask
        return mortality
    else:
        m1, m2 = (param.m1_M, param.m2_M) if stage in Groups.males() else (param.m1_F, param.m2_F)
        anti_mask = np.logical_not(mask) # ~mask not working for non boolean
        if stage == Groups.TENERAL:
            correction = 2. * (mask * param.mort_A + anti_mask * np.ones((param.height, param.width)) )
        else:
            correction = mask * param.mort_A + anti_mask * np.ones((param.height, param.width))
        saturation = np.frompyfunc(lambda x: x if x > 1 else 1., 1, 1)
        threshold = param.T_threshold
        def mortality(temp, cap, pop):
            """Return the mortality rate associated with the specified
            environmental conditions (temperature, capacity,
            population).

            """
            below = temp <= threshold   # boolean mask
            above = temp > threshold
            corr_temp = below * np.full(temp.shape, threshold)\
              + above * temp
            return np.exp(- correction * np.exp(m1*corr_temp + m2) *
                          np.asarray(saturation(pop / cap), dtype=float)), mask
        return mortality

def mortality_rate_natural(stage, param):
    """Return the mortality rate associated with the specified
    stage.

    """
    if stage == Groups.PUP:
        def mortality(temp, cap, pop):
            """Return the mortality rate associated with the specified
            environmental conditions (temperature, capacity,
            population).

            """
            return np.exp(- param.mort_P * param.m_P)
        return mortality
    else:
        m1, m2 = (param.m1_M, param.m2_M) if stage in Groups.males() else (param.m1_F, param.m2_F)
        correction = 2.* param.mort_A if stage == Groups.TENERAL else 1.0* param.mort_A  #* param.mort_A
        saturation = np.frompyfunc(lambda x: x if x > 1 else 1., 1, 1)
        threshold = param.T_threshold
        def mortality(temp, cap, pop):
            """Return the mortality rate associated with the specified
            environmental conditions (temperature, capacity,
            population).

            """
            below = temp <= threshold   # boolean mask
            above = temp > threshold
            corr_temp = below * np.full(temp.shape, threshold)\
              + above * temp
            return np.exp(- correction * np.exp(m1*corr_temp + m2) *
                          np.asarray(saturation(pop / cap), dtype=float))
        return mortality


def load_capacity(path, filename, width, height, proportion = 1.):
    """Return a capacity map based on the filename, with the
    convenient shape (width, height). The file is expected to be found
    in the path directory.

    """
    path = Path(path, filename)
    return proportion * np.fromfile(str(path), sep=' ').reshape((height, width))


def load_temperatures(path, dirname, width, height, offset=1.):
    """Return a list of temperature maps located in the directory,
    with the convenient shape (width, height). The directory is
    expected to be a subdirectory of path.

    """
    path = Path(path, dirname)
    if not path.is_dir():
        return []
    files = [int(filename.stem) for filename in path.iterdir()]
    files.sort()

    temperatures = [np.fromfile(str(path / '{}.txt'.format(number)), sep=' ').reshape((height,width)) + offset
            for number in files]

    return temperatures

class Simulation(object):
    """The Simulation class handles a whole simulation of tse-tse
    population dynamics.

    """
    def __init__(self, parameters, capacity, temperatures, mask, id=1):
        # ID of the simulation
        self.id = id
        # parameters for this simulation
        self.param = parameters
        # size of the environment
        self.width, self.height = parameters.width, parameters.height
        # population capacity of each cell
        self.capacity = capacity
        # temperatures maps (one per day)
        self.temperatures = temperatures
        # mortality rates for each stage
        self.mort_rates_natural = {stage: mortality_rate_natural(stage, self.param)
                                   for stage in Groups.mortal()}
        self.mort_rates_control = {stage: mortality_rate_control(stage, self.param, mask)
                                    for stage in Groups.mortal()}
        # development rates functions for each stage
        self.dev_rates = {stage: devel_rate(stage, self.param)
                          for stage in Groups.developing()}
        # min development rate in the whole simulation (depends on temperatures)
        self.min_devel = {stage: min([rate(t).min() for t in self.temperatures])
                          for stage, rate in self.dev_rates.items()}
        # max development rate (for information)
        self.max_devel = {stage: max([rate(t).max() for t in self.temperatures])
                          for stage, rate in self.dev_rates.items()}
        # max days required to achieve a stage for the lowest development rate
        self.max_days = {stage: math.ceil(1 / min_dev)
                         for stage, min_dev in self.min_devel.items()}
        # corresponding development increment
        self.devel_step = {stage: 1.0 / maxd for stage, maxd in self.max_days.items()}
        # initial population
        self.current_pop = self.init_pop()


        ### initialize outputs
        self.outputs = {
            'sterility ratio': [sterility_ratio(self).mean()],
            'populations': {stage: [self.current_pop[stage].sum()]
                            for stage in Groups},
            'avg temp': [self.temperatures[0].mean()],
            'min temp': [self.temperatures[0].min()],
            'max temp': [self.temperatures[0].max()],
            'last_year': {stage: [] for stage in Groups},
            'lifespan_treated': [],
            'lifespan_not_treated': []
        }


    def init_pop(self):
        """Return the initial population of the model."""
        pop = {stage: np.zeros((self.height,
                                self.width,
                                self.max_days[stage] if stage in self.max_days else 1),
                               dtype=float)
               for stage in Groups}

        if self.param.input_inits == 1:
            for stage in Groups:
                if stage in self.max_days:
                    for dev in range(self.max_days[stage]):
                        my_file = Path(self.param.initial_conditions_dir, stage.name+'_{}.txt'.format(dev))
                        if my_file.is_file():
                            grid = np.fromfile(str(my_file), sep=' ').reshape((self.height, self.width))
                            pop[stage][:,:,dev] = grid
                else:
                    grid = np.fromfile(str(Path(self.param.initial_conditions_dir,stage.name+'_0.txt')), sep=' ').reshape((self.height, self.width))
                    pop[stage][:,:,0] = grid
        else:
            init_common = self.capacity / 2
            pop[Groups.TENERAL][:,:,0] = init_common
            pop[Groups.MALE][:,:,0] = init_common
        return pop


    def total_populations(self):
        """Return the total population per stage."""
        return {stage: self.current_pop[stage].sum()
                for stage in Groups}

    def run_all(self):
        """Run the simulation for the duration specified in the parameters."""
        print("Simulation in progress")
        cd = self.param.control_duration
        d = self.param.duration
        self.additional_stats = False
        if cd != 0:
            if cd < d:
                if cd == 1:
                    self.run_loop(0,365,True)
                    for year in range(int(cd),d):
                        self.additional_stats = year >= d - self.param.stat_years
                        self.run_loop(year*365, (year+1)*365, False)
                elif cd == 0.5:
                    self.run_loop(0,int(cd*364)+1,True)
                    self.run_loop(int(cd*364)+1,365,False)
                    for year in range(1,d):
                        self.additional_stats = year >= d - self.param.stat_years
                        self.run_loop(year*365,(year+1)*365,False)
            elif cd == d: # we can't have cd > d
                for year in range(d):
                    self.additional_stats = year >= d - self.param.stat_years
                    self.run_loop(year*365, (year+1)*365, True)
        else:
            for year in range(d):
                self.additional_stats = year >= d - self.param.stat_years
                self.run_loop(year*365, (year+1)*365, False)

    def run_loop(self, start, stop, control):
        """Run the simulation from the specified start step (included)
        until the specified stop step (excluded).

        """
        y = start//365 + 1
        bar = ChargingBar("Year %d/%d"%(y, self.param.duration), max = (stop - start -1)) # Not adapted for half a year (control)
        for step in range(start, stop):
            self.run(step,control)
            self.update_outputs(step)
            bar.next()
        bar.finish()

    def run(self, step, control):
        """Run the simulation for the specified time step (the
        indication of the time step is required for the
        temperature).

        """
        total_pop = np.sum([self.current_pop[stage].sum(axis=2)
                            for stage in Groups.competitors()], axis=0)

        if self.param.mort_off == 0:
            if control == True:
                self.apply_mortality_control(step, total_pop)
            elif control == False:
                self.apply_mortality_natural(step, total_pop)
        if self.param.dev_off == 0:
            self.current_pop = self.apply_development(step)

        total_pop = np.sum([self.current_pop[stage].sum(axis=2)
                            for stage in Groups.competitors()], axis=0)

        if self.param.mig_off == 0:
            self.apply_migration(step, total_pop)


    def update_outputs(self, step):
        for stage in Groups:
            self.outputs['populations'][stage].append(self.current_pop[stage].sum())
        self.outputs['avg temp'].append(self.temperatures[step % len(self.temperatures)].mean())
        self.outputs['min temp'].append(self.temperatures[step % len(self.temperatures)].min())
        self.outputs['max temp'].append(self.temperatures[step % len(self.temperatures)].max())
        self.outputs['sterility ratio'].append(sterility_ratio(self).mean())
        if self.additional_stats:
            for stage in Groups:
                self.outputs['last_year'][stage].append(self.current_pop[stage].sum(axis=2)) # sum over development

    def apply_mortality_control(self, step, total_pop):
        """Modify the current populations according to the
        environmental conditions.

        """
        for stage in Groups.mortal():
            survival, mask = self.mort_rates_control[stage](self.temperatures[step % len(self.temperatures)],
                                               self.capacity,
                                               total_pop)
            lshape = len(self.current_pop[stage].shape)
            if len(survival.shape) < lshape:
                survival = survival.reshape(survival.shape +
                                              tuple([1] * (lshape - len(survival.shape))))
            self.current_pop[stage] *= survival

            if stage in Groups.females() and stage != Groups.TENERAL:
                treated = np.ma.array(1-survival, mask = np.logical_not(mask))
                mort_treated = treated.mean()
                not_treated = np.ma.array(1-survival, mask = mask)
                mort_not_treated = not_treated.mean()
                self.outputs['lifespan_treated'].append(1/mort_treated)
                self.outputs['lifespan_not_treated'].append(1/mort_not_treated)


    def apply_mortality_natural(self, step, total_pop):
        """Modify the current populations according to the
        environmental conditions.

        """
        for stage in Groups.mortal():
            survival = self.mort_rates_natural[stage](self.temperatures[step % len(self.temperatures)],
                                               self.capacity,
                                               total_pop)
            lshape = len(self.current_pop[stage].shape)
            if len(survival.shape) < lshape:
                survival = survival.reshape(survival.shape +
                                              tuple([1] * (lshape - len(survival.shape))))
            self.current_pop[stage] *= survival

            if stage in Groups.females() and stage != Groups.TENERAL:
                not_treated = 1 - survival
                mort_not_treated = not_treated.mean()
                self.outputs['lifespan_not_treated'].append(1/mort_not_treated)



    def apply_development(self, step):
        """Compute the new population structure according to the local
        development rate of each cell.

        """
        developing = list(Groups.developing())
        others = [stage for stage in Groups if stage not in developing]
        # compute the "development translation" associated to each cell
        # i.e. how many cells the flies "move forward" in their development grid
        current_temp = self.temperatures[step % len(self.temperatures)]
        cell_inf = {stage: np.asarray(np.floor_divide(self.dev_rates[stage](current_temp),
                                                              self.devel_step[stage]), dtype = int)
                     for stage in developing}
        cell_true = {stage: np.asarray(np.true_divide(self.dev_rates[stage](current_temp),
                                                              self.devel_step[stage]))
                    for stage in developing}
        prop_sup = {stage: cell_true[stage] - cell_inf[stage] for stage in developing}

        # initialize next pop with zeros
        next_pop = {stage: np.zeros((self.height, self.width, self.max_days[stage]), dtype=float)
                    for stage in developing}
        # add non-developping stages
        for stage in others:
            next_pop[stage] = self.current_pop[stage]

        ### egg layers are expected to be a subset of developping stages
        egg_layers = list(Groups.laying_eggs())

        # for each stage susceptible of moving towards the stage group:
        for stage in developing:
            next_stages = stage.next_stages(self)
            # check each cell
            for y in range(self.height):
                for x in range(self.width):
                    # the population at devel level 0 in the upper
                    # stage is the sum of all populations which would
                    # "leave" the development array by a translation

                    metamorph_inf = self.current_pop[stage][y, x, -cell_inf[stage][y, x]:].sum()
                    metamorph = metamorph_inf + prop_sup[stage][y,x]*self.current_pop[stage][y, x, -(cell_inf[stage][y, x]+1)]

                    for (next_stage, proportion) in next_stages:
                            next_pop[next_stage][y, x, 0] +=  proportion[y, x]  * metamorph
                    if stage in egg_layers:
                        next_pop[Groups.eggs()][y, x, 0] += metamorph


        # compute ordinary development, i.e. a translation in each development array (pop[y, x]) :

        for stage in Groups.developing():
            for y in range(self.height):
                for x in range(self.width):

                    next_pop[stage][y, x, cell_inf[stage][y, x]:] += self.current_pop[stage][y, x, 0:-cell_inf[stage][y, x]] * (1-prop_sup[stage][y,x])

                    next_pop[stage][y, x, (cell_inf[stage][y, x]+1):] += self.current_pop[stage][y, x, 0:-(cell_inf[stage][y, x]+1)] * prop_sup[stage][y,x]



        return next_pop

    def apply_migration(self, step, total_pop):
        """Make all stages move to adjacent cells depending on their
        attractivity.

        """
        migration = 1/(1 + np.exp(-self.param.gamma * (total_pop/self.capacity - 1)))
        attractivity = (1 - np.exp(-self.capacity / total_pop)) * self.capacity
        for stage in Groups.migrating():
            diffuse2d_general(self.current_pop[stage],
                              attraction=attractivity,
                              diffusion=migration,
                              radius=self.param.radius_disp)










