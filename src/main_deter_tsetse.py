"""
Version 2.0
Hereafter is the code to parse arguments, generate outputs and run simulations
of the tsetse flies population dynamics model.
Associated files are tools_tsetse.py and model_deter_tsetse.py

Contributors and contact:
-------------------------

    - Hélène Cecilia (helene.cecilia[at]oniris-nantes.fr)
    - Sandie Arnoux (sandie.arnoux[at]inra.fr)
    - Sébastien Picault (sebastien.picault[at]oniris-nantes.fr)
    - Pauline Ezanno (pauline.ezanno[at]inra.fr)

    BIOEPAR, INRA Oniris, Atlanpole La Chantrerie
    Nantes CS 44307 CEDEX, France



License:
--------
CeCILL-B
IDDN (Inter Deposit Digital Number) : coming soon

"""
import warnings

import numpy                      as np
from   math                       import ceil
from   pathlib                    import Path

from   tools_tsetse import *
from   model_deter_tsetse import Simulation, Groups, load_capacity, load_temperatures
import os
import pandas as pd

"""
Parameters definition and reference value
"""
DEFAULT_PARAM = StateVarDict(
    duration=1,                 # duration in years
    width=30,                   # width of the environment (cells of 250m x 250m)
    height=30,                  # height of the environment (cells of 250m x 250m)
    path='input/environment/',  # path to temperature and capacity data
    cap_file='K30.txt',         # name of the file containing capacity data
    temp_dir='temp30',          # name of the dir holding temperature data
    output_dir='outputs/ref/',  # directory for storing outputs
    initial_conditions_dir = 'outputs/initial_state/', #directory for initial conditions
    d1_P = 106.2264,            #
    d2_P = 18.9483,             #
    d3_P = 20.2279,             #
    d4_P = 2.4901,              #
    d1_T=0.002,                 # d1_N (nulliparous, previously called teneral)
    d2_T=0.061,                 # d2_N (nulliparous, previously called teneral)
    d1_F=0.0052,                #
    d2_F=0.1046,                #
    m_P=0.01,                   #
    m2_F=-12.94,                #
    m1_F=0.358,                 #
    m2_M=-9.53,                 #
    m1_M=0.254,                 #
    gamma=10.,                  #
    stat_years=1,               # nb of years for computing stats
    T_0=24.,                    #
    T_threshold=24.,            # temperature threshold in mortality rates
    T_modif=0.,                 # offset for temperature adjustment
    K_modif = 1.,               # proportion of estimated carrying capacity grid
    sterility_penality = 3.,    # number of sterile males required to
                                # counterbalance one fertile male
    radius_disp=1,              # dispersion radius
    input_inits = 0,            # automatic initial condition or from directory
##### Sensitivity Analysis ######
    mort_A = 1.,                # proportion of mortality function applied to adults (T,F,M)
    mort_P = 1.,                # proportion of mortality function applied to pupae
    dev_P = 1.,                 # proportion of development function applied to pupae
    dev_T = 1.,                 # dev_N proportion of development function applied to nulliparous females
    dev_F = 1.,                 # proportion of development function applied to parous females

##### Turn-off biological processes
    mort_off = 0,
    dev_off = 0,
    mig_off = 0,

##### Algo for control optimization
    target_pop = 0.05,          # objective: population decrease needed (% of initial population)
    tolerance = 0.001,          # tolerance to consider the target reached (target_pop +- tolerance)
    control_duration = 0,       # duration to apply increased mortality (year)
    search_step = 0.1,          # mortality added at each step
#    pxl_trt = [100],           # TO DO : list of proportions of controlled cells to optimize

    input_params_file = "PAR_test.txt", # file containing parameters
    output_aggreg_file = "SOR_ref.txt" # file for aggregated outputs
)

"""
Saves number of individuals per stage per time step
Summed over the grid, and over development steps
"""
def output_pops(simulation, sep=' '):
    directory = str(Path(simulation.param.output_dir,"time"))
    if not os.path.exists(directory):
        os.makedirs(directory)

    headers = sep.join([stage.name for stage in Groups])
    filename = str(Path(directory,"pop.txt"))
    np.savetxt(filename,
               np.array([simulation.outputs['populations'][stage] for stage in Groups]).T,
               header=headers, comments="")

# Saves the final state of the simulation
def output_ends(simulation):
    directory = str(Path(simulation.param.output_dir,
                                'final_state{}'.format(simulation.id)))
    if not os.path.exists(directory):
        os.makedirs(directory)
    for stage in Groups:
        if stage in simulation.max_days:
            for dev in range(simulation.max_days[stage]):
                filename = str(Path(directory,
                                stage.name+'_{}.txt'.format(dev)))
                np.savetxt(filename, simulation.current_pop[stage][:,:,dev])
        else:
            dev = 0
            filename = str(Path(directory,
                            stage.name+'_{}.txt'.format(dev)))
            np.savetxt(filename, simulation.current_pop[stage][:,:,dev])
"""
Saves outputs used for sensitivity analysis
"""
def output_sensitivity_FAST_article(simulation):
    concat_flying_females = np.sum([np.mean(simulation.outputs['last_year'][stage], axis=0)
                            for stage in Groups.females()], axis=0)
    mean_flying_females_cell = np.mean(concat_flying_females)
    std_flying_females_cell = np.std(concat_flying_females)

    sum_grid_flying_females = np.sum([[np.sum(grid_day) for grid_day in simulation.outputs['last_year'][stage]]
                            for stage in Groups.females()], axis=0)
    mean_flying_females_grid = np.mean(sum_grid_flying_females)
    std_flying_females_grid = np.std(sum_grid_flying_females)

    concat_Na = concat_flying_females + np.sum([np.mean(simulation.outputs['last_year'][stage], axis=0) for stage in Groups.males()], axis = 0)

    repartition = concat_Na / simulation.capacity
    mean_distrib = np.mean(repartition)
    std_distrib = np.std(repartition)
    q5_distrib = np.percentile(repartition,5)
    q25_distrib = np.percentile(repartition,25)
    q75_distrib = np.percentile(repartition,75)
    q95_distrib = np.percentile(repartition,95)
    median_distrib = np.percentile(repartition,50)
    NK_10 = repartition >= 0.1 # boolean mask, relative threshold
    NK_90 = repartition >= 0.9 # boolean mask, relative threshold
    NK_80 = repartition >= 0.8 # boolean mask, relative threshold
    NK_20 = repartition >= 0.2 # boolean mask, relative threshold
    NK_50 = repartition >= 0.5 # boolean mask, relative threshold
    NK_70 = repartition >= 0.7 # boolean mask, relative threshold
    NK_75 = repartition >= 0.75 # boolean mask, relative threshold
    surf_totale = (simulation.width * simulation.height)#* 250 * 250
    surface_presence_10 = np.sum(NK_10) / surf_totale
    surface_presence_90 = np.sum(NK_90) / surf_totale
    surface_presence_80 = np.sum(NK_80) / surf_totale
    surface_presence_20 = np.sum(NK_20) / surf_totale
    surface_presence_50 = np.sum(NK_50) / surf_totale
    surface_presence_70 = np.sum(NK_70) / surf_totale
    surface_presence_75 = np.sum(NK_75) / surf_totale


    mini_one_adult_stages = np.zeros((simulation.height, simulation.width))
    for stage in Groups.competitors():
        mini_one_adult_stages += np.mean(simulation.outputs['last_year'][stage], axis = 0) >= 1

    surface_presence_1 = np.sum(mini_one_adult_stages > 0)/ (simulation.width * simulation.height)
    min_propA1 = []
    min_propA2 = []
    mean_propA1 = []
    mean_propA2 = []
    max_propA1 = []
    max_propA2 = []
    for a1,a2,a3 in zip(simulation.outputs['last_year'][Groups.ADULT1], simulation.outputs['last_year'][Groups.ADULT2], simulation.outputs['last_year'][Groups.ADULT3]):
        propA1 = a1 / (a1 + a2 + a3)
        propA2 = a2 / (a1 + a2 + a3)
        min_propA1.append(np.min(propA1))
        min_propA2.append(np.min(propA2))
        mean_propA1.append(np.mean(propA1))
        mean_propA2.append(np.mean(propA2))
        max_propA1.append(np.max(propA1))
        max_propA2.append(np.max(propA2))

    mi1 = np.min(min_propA1) * 100
    mi2 = np.min(min_propA2) * 100
    me1 = np.mean(mean_propA1) * 100
    me2 = np.mean(mean_propA2) * 100
    ma1 = np.max(max_propA1) * 100
    ma2 = np.max(max_propA2) * 100

    sor = [mean_flying_females_cell, std_flying_females_cell, surface_presence_10, surface_presence_20, surface_presence_50,
           surface_presence_75, surface_presence_80, surface_presence_90, surface_presence_1, mi1, me1, ma1, mi2, me2, ma2,
           mean_distrib, std_distrib, q5_distrib, q25_distrib, q75_distrib, q95_distrib, median_distrib]
    sor = [str(i) for i in sor]
    a = " "

    directory = str(Path(simulation.param.output_dir,"aggregated"))
    if not os.path.exists(directory):
        os.makedirs(directory)

    filename = str(Path(directory,simulation.param.output_aggreg_file))
    with open(filename,"a") as f:
        f.write(a.join(sor)+"\n")

"""
Plots number of individuals over time
"""
def plot_pops(simulation):
    ############################# Plot and save image #########################

    DURATION_VIEW = simulation.param.duration*365
    plt.clf()
    fig, ax = plt.subplots(1, 1, figsize=(9, 6))
    # Remove the plot frame lines. They are unnecessary here.
    ax.set_facecolor("white")
    ax.yaxis.get_major_formatter().set_powerlimits((0, 1))
    plt.grid(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # Ensure that the axis ticks only show up on the bottom and left of the plot.
    # Ticks on the right and top of the plot are generally unnecessary.
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

    # Limit the range of the plot to only where the data is.
    # Avoid unnecessary whitespace.
    ymax = max([max(simulation.outputs['populations'][stage][:DURATION_VIEW]) for stage in Groups])
    #print(ymax)
    plt.xlim(-0.5, DURATION_VIEW+1)
    plt.ylabel('Number of individuals')
    plt.xlabel('Time (days)', fontsize=14)
    # Make sure your axis ticks are large enough to be easily read.
    # You don't want your viewers squinting to read your plot.
    yrng = range(0, ceil(1.2 * ymax), 100000)
    xrng = range(0, DURATION_VIEW+1, 200)
    plt.xticks(xrng, fontsize=13)
    plt.yticks(yrng, ['{:.1f}'.format(x) for x in yrng], fontsize=13)

    plots = []
    for stage in [Groups.TENERAL, Groups.ADULT1, Groups.ADULT2, Groups.ADULT3, Groups.ADULT4, Groups.MALE]: #Groups.PUP,
        p, = plt.plot(np.arange(DURATION_VIEW), simulation.outputs['populations'][stage][:DURATION_VIEW],
                     '.-' if DURATION_VIEW < 200 else '-', lw=1, color=stage.color, label=stage.name)
        plots.append(p)

    # Note that if the title is descriptive enough, it is unnecessary to include
    # axis labels; they are self-evident, in this plot's case.
    plt.title('Evolution of the population of each stage over time ')

    llabs = [stage.name for stage in Groups]
    lgd = plt.legend(handles=plots, bbox_to_anchor=[0.5, 1.2], fontsize=10,
               ncol=4, shadow=True, title="Stages", fancybox=True,
               loc = "center")
    lgd.get_frame().set_facecolor('white')

    ax2 = ax.twinx()
    ax2.grid(False)
    ax2.plot(np.arange(DURATION_VIEW), simulation.outputs['avg temp'][:DURATION_VIEW], ':', lw=2, color='darkgrey')

    ax2.set_ylabel('Temperature', color='darkgrey')
    for tl in ax2.get_yticklabels():
        tl.set_color('darkgrey')
        ax2.set_ylim([18,30])
    # Finally, save the figure as a PNG.
    # You can also save it as a PDF, JPEG, etc.
    # Just change the file extension in this call.

    plt.savefig(str(Path(simulation.param.output_dir,'evolution_{}.png'.format(simulation.id))), bbox_inches='tight')

"""
Saves the spatial distribution of female stages (summed)
at three time steps
"""
def output_maps(simulation):
    directory = str(Path(simulation.param.output_dir,"space"))
    if not os.path.exists(directory):
        os.makedirs(directory)

    for i in [0, 182, 364]: #range(0,365): 
        maps = np.sum([simulation.outputs['last_year'][stage][i]
                   for stage in Groups.females()], axis=0)
        mapname = str(Path(directory,"map_day{}.txt".format((i+1))))
        np.savetxt(mapname, maps)

"""
For a given set of controlled cells
search the minimum mortality rate to apply in these cells
to achieve the target population
"""
def search_optimal_mortality(parameters, no_control, mort_start, MASK, prop):
    goal = ""
    mort_A = mort_start
    mort_max = 3.1
    mort_min = 0
    # maximum control effort allowed (to avoid unnecessary iterations to achieve the target population)
    mort_MAX = 3.1
    step = parameters.search_step # see if it can evolve to speed up the search
    while goal != "goal":
        # create the simulation
        parameters.mort_A = mort_A
        simulation = Simulation(parameters, capacity=CAPACITY, temperatures=TEMPERATURES, mask=MASK, id = parser.args.id)
        # run the simulation
        simulation.run_all()
        goal, final_ratio = check_target_control_achieved(simulation, no_control)
        if goal == "high":
            mort_min = mort_A
            delta = min(step, (mort_max-mort_A)/2)
            mort_A = mort_A + delta
        elif goal == "low":
            mort_max = mort_A
            delta = min(step, (mort_A - mort_min)/2)
            mort_A = mort_A - delta
        print("Prop: "+str(prop)+", mort_A tested: "+str(parameters.mort_A)+" / result achieved: "+str(final_ratio)+" ("+goal+")")
        print("Next mort_A: "+str(mort_A))
        if mort_A >= mort_MAX - 0.1: #if the tested proportion cannot achieve the goal with a sensible mortality, stop searching (TO DO: try with smaller proportion)
            goal = "impossible"
            break
    if goal == "goal":
        filename = "stat"+str(prop)+".txt"
        filepath = str(Path(simulation.param.output_dir,filename))
        res = open(filepath, "a")
        mean_treated = np.mean(simulation.outputs['lifespan_treated'])
        mean_not_treated = np.mean(simulation.outputs['lifespan_not_treated'])
        res.write(str(parameters.mort_A) + " " + str(final_ratio) + " " + str(mean_treated) + " " + str(mean_not_treated) + "\n")
        res.close()

        output_maps(simulation)
        output_pops(simulation)
        plot_pops(simulation)
        output_ends(simulation)

        return parameters.mort_A, goal
    else:
        return mort_start, goal


"""
Check against no-control scenario
if target population is achieved after control
"""
def check_target_control_achieved(simulation, no_control):
    control_duration = simulation.param.control_duration
    target_pop = simulation.param.target_pop
    tolerance = simulation.param.tolerance
    check_time = int(control_duration*364)
    scenario = np.sum(simulation.outputs['populations'][stage][check_time] for stage in Groups.females())
    ratio = scenario/no_control
    if target_pop - tolerance <= scenario/no_control  <= target_pop + tolerance: #tolerance*target_pop
        return "goal",ratio
    elif scenario/no_control  >= target_pop + tolerance:
        return "high",ratio
    elif target_pop - tolerance >= scenario/no_control:
        return "low",ratio

"""
Chose cells to control:
cells most impacted, quantified by Pij score
Create the mask accordingly
"""
def compute_control_mask(prop, control_ref, parameters):
    weight = np.ones((parameters.height, parameters.width))
    directory = str(Path(parameters.output_dir,"space"))
    if not os.path.exists(directory):
        os.makedirs(directory)
    if prop == 100:
        mask = True * weight
        maskname = str(Path(directory,"mask.txt"))
        np.savetxt(maskname, mask)
        return mask
    else:
        check_time = int(parameters.control_duration * 364)
        map_control = np.genfromtxt(control_ref +"space/map_day" + str(check_time + 1) + ".txt")
        mask_control = np.genfromtxt(control_ref +"space/mask.txt")
        sum_control = np.sum(map_control)
        map_no_control = np.genfromtxt("outputs/ref_1y/space/map_day"+ str(check_time + 1) + ".txt") #_no_migration

        for (x,y),pxl_no_control in np.ndenumerate(map_no_control):
            weight[x,y] = pxl_no_control + sum_control - map_control[x,y]

        # Save Pij scores
        weightname = str(Path(directory,"Pij.txt"))
        np.savetxt(weightname, weight)
        # Cells previously identified as not treated have a zero score (double check)
        final_weight = mask_control * weight + np.logical_not(mask_control) * np.zeros((parameters.height, parameters.width))
        # Threshold to treat cells is defined as the percentile of scores corresponding to the proportion we want to treat
        threshold = np.percentile(final_weight, 100-prop)
        # Controlled cells are the ones with a score above the threshold
        mask = final_weight >= threshold
        maskname = str(Path(directory,"mask.txt"))
        np.savetxt(maskname, mask)

        scorename = str(Path(directory,"Pij.txt"))
        np.savetxt(scorename, weight)

        return mask


if __name__ == '__main__':
    parser = EmulsionParser(DEFAULT_PARAM, version='deterministic 1.1')
    parser.add_argument('--map',
                        action='store_true',
                        dest='output_maps',
                        help='Map (spatial distribution) of females (N+F) at days d = [1, 183, 365] the last year of simulation.\n \
                                Saved in <output_dir>/space as map<id>_day<d>.txt\n \
                                Default <output_dir> is outputs/ref/\n \
                                Default <id> is 1.')
    parser.add_argument('--pop',
                        action='store_true',
                        dest='output_pops',
                        help='Number of individuals over time for each stage, aggregated over the grid.\n \
                                Space-separated, with header \n \
                                Saved in <output_dir>/time/ as pop_<id>.txt\n \
                                Default <output_dir> is outputs/ref/\n \
                                Default <id> is 1.')
    parser.add_argument('--end',
                        action = 'store_true',
                        dest = 'output_ends',
                        help = 'Stores the final state of the system in <output_dir>/final_state<id>/ \n \
                                One file per stage per development step \n \
                                This state can be used as initial condition for future simulations with option -p input_inits=1\n \
                                and -p initial_conditions_dir=<path>, default /outputs/initial_state/ \n \
                                Default <output_dir> is outputs/ref \n \
                                Default <id> is 1.')
    parser.add_argument('--agg',
                        action = 'store_true',
                        dest = 'output_sensitivity_FAST_article',
                        help = 'Write aggregated outputs in <output_dir>/aggregated/<output_aggreg_file> (space separated, no header).\n \
                                Default <output_dir> is outputs/ref/\n \
                                Default <output_aggreg_file> is SOR_ref.txt\n \
                                Current outputs and order :\n \
                                popMean, popStd, surf10, surf20, surf50, surf75, surf80, surf90, surf1,\n \
                                mi1, me1, ma1, mi2, me2, ma2, mean_distrib, std_distrib,\n \
                                q5_distrib, q25_distrib, q75_distrib, q95_distrib, median_distrib')
    parser.add_argument('--plot',
                        action = 'store_true',
                        dest = 'plot_pops',
                        help = 'Plot population over time aggregated over the grid. Different stages represented. Saved in <output_dir> as evolution_<id>.png\n \
                                Default <output_dir> is outputs/ref/\n \
                                Default <id> is 1.')
    parser.add_argument('--all',
                        action = 'store_true',
                        dest = 'all',
                        help = 'Same as --pop --plot --map\n \
                                In addition a file stat.txt is created containing the mean lifespan of parous females.')
    parser.add_argument('--control_opti',
                        action = 'store_true',
                        dest = 'control_opti',
                        help = 'For successive proportions of pixels treated, finds the most relevant pixels to treat, \n \
                                then looks for the minimal mortality rate needed to achieve \n \
                                <target_pop> at the end of <control_duration>')
    parser.add_argument('--fi',
                    action = 'store_true',
                    dest = 'input_from_file',
                    help = 'Read parameters from input/scenario/<input_params_file> (default PAR_tests.txt)\n \
                            One simulation launched per line in the file\n \
                            <input_params_file> structure : space-separated, no header\n \
                            Current order of params: mort_P, dev_P, dev_T, mort_A, dev_F, T_modif, gamma, K_modif\n \
                            Can be generated by scripts/sensitivity_analysis/FAST_param_and_results.R')


    parser.do_parsing()

    parameters = parser.parameters
    WIDTH, HEIGHT = parameters.width, parameters.height
    CAPACITY = load_capacity(parameters.path,
                             parameters.cap_file,
                             WIDTH, HEIGHT, proportion = parameters.K_modif)
    TEMPERATURES = load_temperatures(parameters.path,
                                     parameters.temp_dir,
                                     WIDTH, HEIGHT,
                                     offset=parameters.T_modif)

#    directory = str(Path(parameters.output_dir))
#    if not os.path.exists(directory):
#        os.makedirs(directory)
    if parser.args.input_from_file:
        with open(str(Path("input/scenario",parameters.input_params_file))) as f:
            for line in f:
                line = line.strip('\n')
                scenario = line.split(" ")
                scenario = [float(i) for i in scenario]
                parameters.mort_P = scenario[0]
                parameters.dev_P = scenario[1]
                parameters.dev_T = scenario[2]
                parameters.mort_A = scenario[3]
                parameters.dev_F = scenario[4]
                parameters.T_modif = scenario[5]
                parameters.gamma = scenario[6]
                parameters.K_modif = scenario[7]

                CAPACITY_ = CAPACITY * parameters.K_modif
                TEMPERATURES_ = [T + parameters.T_modif for T in TEMPERATURES]
                MASK = np.ones((parameters.height, parameters.width))
                # init the simulation
                simulation = Simulation(parameters, capacity=CAPACITY_, temperatures=TEMPERATURES_,
                                        mask = MASK, id = parser.args.id)
                print("Lauching scenario")
                # run the simulation
                simulation.run_all()
    #                print({func: np.mean(vals) for func, vals in simulation.TIMES.items()})
    #                print("Durée d'exécution pour 4 ans : {:.1f} minutes".format(np.mean(TIMES['run_all']) * 4 * 365 / 60))

                if parser.args.output_sensitivity_FAST_article:
                    output_sensitivity_FAST_article(simulation)
    elif parser.args.control_opti:
        if parameters.mig_off == 0:
            suffix = ""
        else:
            suffix = "_no_mig"
        ref = pd.read_csv("outputs/ref_1y"+suffix+"/time/pop.txt", sep=" ")
        ref["FEMALES"] = ref[["TENERAL","ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
        check_time = int(parameters.control_duration*364)
        no_control = ref["FEMALES"][check_time]
        mort_start = parameters.mort_A
        control_ref = "outputs/pixel100_duration"+str(check_time+1)+"_obj"+str(int(100*parameters.target_pop))+suffix+"/" 
        parameters.pxl_trt = [100,90,80,70,60,55,54,53,52,51,50,49,48,47,46,45,40,35,34,33,32,31,30]
        for prop in parameters.pxl_trt:
            parameters.output_dir = "outputs/pixel"+str(prop)+"_duration"+str(check_time+1)+"_obj"+str(int(100*parameters.target_pop))+suffix+"/" 
            mask = compute_control_mask(prop, control_ref, parameters)
            # Find optimal mortality rate for one proportion of pixels treated. Start from this rate for the next search (lower proportion)
            mort_prev, goal = search_optimal_mortality(parameters, no_control, mort_start, mask, prop)
            if goal == "impossible":
                print("impossible prop: "+str(prop))
                break
            mort_start = mort_prev
            # The result directory becomes the control_ref for the next search
            control_ref = parameters.output_dir
    else:
        MASK = np.ones((parameters.height, parameters.width))
        # init the simulation
        simulation = Simulation(parameters, capacity = CAPACITY, temperatures = TEMPERATURES, mask = MASK, id = parser.args.id)

        # run the simulation
        simulation.run_all()
        outputs = 0
        if parser.args.all:
            outputs = 1
            output_maps(simulation)
            output_pops(simulation)
            plot_pops(simulation)

            filename = "stat.txt"
            filepath = str(Path(simulation.param.output_dir,filename))
            res = open(filepath, "a")
            mean_not_treated = np.mean(simulation.outputs['lifespan_not_treated'])
            res.write(str(mean_not_treated))
            res.close()
        else:
            if parser.args.output_pops:
                outputs = 1
                output_pops(simulation)
            if parser.args.output_ends:
                outputs = 1
                output_ends(simulation)
            if parser.args.output_sensitivity_FAST_article:
                outputs = 1
                output_sensitivity_FAST_article(simulation)
            if parser.args.plot_pops:
                outputs = 1
                plot_pops(simulation)
            if parser.args.output_maps:
                outputs = 1
                output_maps(simulation)
        if outputs == 0:
            print("No output type detected in command line arguments. Nothing was saved. Why not try --all ?")
