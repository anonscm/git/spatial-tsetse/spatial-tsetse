"""
Version 1.0
Hereafter are useful tools for the model of tsetse flies population dynamics.
Associated files are main_deter_tsetse.py and model_deter_tsetse.py

Contributors and contact:
-------------------------

    - Hélène Cecilia (helene.cecilia[at]oniris-nantes.fr)
    - Sandie Arnoux (sandie.arnoux[at]inra.fr)
    - Sébastien Picault (sebastien.picault[at]oniris-nantes.fr)
    - Pauline Ezanno (pauline.ezanno[at]inra.fr)

    BIOEPAR, INRA Oniris, Atlanpole La Chantrerie
    Nantes CS 44307 CEDEX, France



License:
--------
CeCILL-B
IDDN (Inter Deposit Digital Number) : coming soon

"""
from   enum                       import Enum, unique
import sys

from   argparse                   import ArgumentParser, ArgumentTypeError

import numpy                as np
### This was added due to an error with _tkinter DISPLAY (to generate .png files)
import matplotlib
matplotlib.use('Agg')
###

import matplotlib.pyplot    as plt
import matplotlib.animation as animation

import time
from   functools                  import wraps

######################################################################################
######################################################################################

""" Tools for performance assessment.
"""
def timethis(times=None):
    """A decorator function for printing execution time."""
    def timefunc(func):
        """The actual decorator"""
        @wraps(func)
        def wrapper(*args, **kwargs):
            """Decorate the specified function in order to measure its
            execution time. Execution times are stored in the TIMES
            dictionary.

            """
            start = time.perf_counter()
            result = func(*args, **kwargs)
            end = time.perf_counter()
            if times is None:
                print('{}.{} : {}'.format(func.__module__,
                                          func.__name__,
                                          end - start))
            else:
                if func.__name__ not in times:
                    times[func.__name__] = [end-start]
                else:
                    times[func.__name__].append(end-start)
            return result
        return wrapper
    return timefunc
######################################################################################
######################################################################################

class StateVarDict(dict):
    """A special dictionary aimed at handling the State Variables of a
    model and providing an attribute-like access syntax.

    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self

@unique
class AutoEnum(Enum):
    """AutoEnums are enumerations where the value of each element is
    computed automatically (the nth item gets value n), whatever
    constructor is used to instantiate the items of the
    enumeration. AutoEnums are expected to define unique elements.

    """
    def __new__(cls, *args, **kwargs):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj
######################################################################################
######################################################################################

class EmulsionParser(ArgumentParser):
    """Tools for providing a generic command line parser with useful options.
    """
    def __init__(self, parameters, version='1.0', **kwargs):
        super().__init__(**kwargs)
        # define default values
        self.parameters = StateVarDict(parameters)
            # define line command arguments
        self.add_argument('-v', '--version',
                          action='version',
                          version='%(prog)s {}'.format(version),
                          help='Print the version of the program and exit.')
        self.add_argument('--show-params',
                          action='store_true',
                          dest='show_params',
                          help='Show the name of available parameters and exit.')
        self.add_argument('-y', '--years',
                          dest='years',
                          help='Set the number of years to simulate. Same as "-p duration=value".\n \
                                Must be an integer.')
        self.add_argument('-p', '--param',
                          action='append',
                          dest='param',
                          help='Specify a value for a parameter, instead of the default value.')
        self.add_argument('-i', '--id',
                          dest='id',
                          help='Specify the ID of the simulation')

    def do_parsing(self):
        self.args = self.parse_args()
        if self.args.years:
            self.parameters.duration = int(self.args.years)

        if self.args.param:
            for key, val in [p.split('=') for p in self.args.param]:
                if key not in self.parameters:
                    print('Unknown parameter', key)
                    sys.exit(-1)
                else:
                    self.parameters[key] = type(self.parameters[key])(val)

        if self.args.show_params:
            print('Available parameters are (with their default value):')
            for key, val in self.parameters.items():
                print('    {:.<24}{:.>20}'.format(key, val))
            sys.exit()

######################################################################################
######################################################################################
def diffuse2d_general(z_orig,
                      attraction,
                      diffusion=np.array([0.5]),
                      radius=1,
                      wrapx=False,
                      wrapy=False):
    """In-place anisotropic diffusion with arbitrary radius (based on
    the Moore distance), including the original cell as a possible
    destination. The anisotropic diffusion consists of replacing a
    part ('diffusion' parameter) of each value of the z_orig field by
    a contribution of the neighboring values according to their
    attractivity, specified in a separate matrix.

    """
    height, width = z_orig.shape[:2]
    padshape = ((radius, radius), (radius, radius)) + tuple([(0, 0)
                                         for _ in range(len(z_orig.shape)-2)])
    # if more than 2D, reshape attraction and diffusion if needed
    if len(z_orig.shape) > len(attraction.shape):
        attraction = attraction.reshape(attraction.shape +
                                        tuple([1] * (len(z_orig.shape)-len(attraction.shape))))
        if not isinstance(diffusion, float):
            diffusion = diffusion.reshape(diffusion.shape +
                                          tuple([1] * (len(z_orig.shape)-len(diffusion.shape))))

    # compute attraction "around" each cell (wrt wrapping)
    att = np.pad(attraction, padshape, mode='wrap')
    if not wrapx:
        att[:, 0:radius] = 0
        att[:, (width+radius):(width + 2*radius)] = 0
    if not wrapy:
        att[0:radius, :] = 0
        att[(height+radius):(height + 2*radius), :] = 0

    # compute total attraction in the neighborhood of each cell
    neighbors_att = np.zeros(z_orig.shape, dtype=float)
    for dy in range(2*radius + 1):
        for dx in range(2*radius + 1):
            neighbors_att += att[dy:(dy + height), dx:(dx + width)]

    # since neighbors_att is used to normalize the attactivity of
    # neighboring cells, ensure that it is not 0
    neighbors_att[neighbors_att == 0] = 1.

    # ratio available for migration (leaving its original cell)
    available = np.pad(z_orig * diffusion / neighbors_att, padshape, mode='wrap')
    if not wrapx:
        available[:, 0:radius] = 0
        available[:, (width+radius):(width + 2*radius)] = 0
    if not wrapy:
        available[0:radius, :] = 0
        available[(height+radius):(height + 2*radius), :] = 0

    # quantity which actually moves towards each cell
    migration = np.zeros(z_orig.shape, dtype=float)
    for dy in range(2*radius + 1):
        for dx in range(2*radius + 1):
            migration += available[dy:(dy + height), dx:(dx + width)]
    migration *= attraction

    z_orig *= 1.0 - 1.0*diffusion
    z_orig += migration


######################################################################################
######################################################################################
""" Tools for data/map visualization
"""
def show_img(value, cmap='hot', save=None, colbar=True, **kwargs):
    """Display the specified value as an image. A special color map
    name can be specified, as well as the presence of a colorbar and
    additional keyword arguments passed to ``imshow``. If a filename is
    provided in the ``save`` parameter, the image is stored in that
    file.

    """
    plt.imshow(value, cmap=plt.get_cmap(cmap), interpolation='none', **kwargs)
    if colbar:
        plt.colorbar()
    if save:
        plt.savefig(save)
    plt.show()


def show_contour(value, cmap='hot', save=None, colbar=False, **kwargs):
    """Display the specified value as a contour map with values. A
    special color map name can be specified, as well as the presence
    of a colorbar and additional keyword arguments passed to
    ``contourf``. If a filename is provided in the ``save`` parameter, the
    image is stored in that file.

    """
    plt.contourf(value, cmap=cmap, **kwargs)
    cont = plt.contour(value, colors='black', alpha=0.75)
    plt.clabel(cont, fmt='%2.1f', colors='black', fontsize=6)
    plt.axes().set_aspect('equal')
    if colbar:
        plt.colorbar()
    if save:
        plt.savefig(save)
    plt.show()


def show_histo(value, xlabel, facecol='green', save=None):
    """Display the distribution of the specified ``value`` array using
    ``xlabel`` for the legend. A specific face color can be used. If a
    filename is provided in the ``save`` parameter, the image is
    stored in that file.

    """
    plt.hist(np.ravel(value), 50, facecolor=facecol, alpha=0.75)
    plt.xlabel(xlabel)
    plt.ylabel('Nb cells')
    plt.title('Histogram of {}'.format(xlabel))
    plt.grid(True)
    if save:
        plt.savefig(save)
    plt.show()


def build_animation(values, title, unit=None, filename=None,
                    cmap='coolwarm', framerate=10, resolution=100,
                    writer='imagemagick', **kwargs):
    """Create an animation based on the ``values`` list, with the
    specified title. A special color map name can be specified, as
    well as the presence of a colorbar and additional keyword
    arguments passed to ``imshow``. If a specified unit is given, each
    frame is marked with its number and the corresponding unit. If a
    filename is provided in the ``save`` parameter, the animation is
    stored in that file.

    """
    ims = []
    fig = plt.figure(title)
    ax = fig.add_subplot(111)

    for img_num, value in enumerate(values):
        frame = plt.imshow(value, cmap=plt.get_cmap(cmap), animated=True, **kwargs)
        if unit:
            text = ax.annotate("{} {:03}".format(unit, img_num + 1), (1, 1))
            ims.append([frame, text])
        else:
            ims.append([frame])

    ani = animation.ArtistAnimation(fig, ims, interval=350, blit=True, repeat_delay=350)
    if filename:
        ani.save(filename, writer=writer, fps=framerate, dpi=resolution)

    plt.colorbar()
    plt.show()
