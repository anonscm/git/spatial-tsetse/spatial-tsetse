#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 10:45:06 2019

@author: hcecilia
"""
import matplotlib.pyplot as plt
from matplotlib import gridspec as gs
import numpy as np

import numpy.ma as ma

import pandas as pd
from matplotlib import rc, rcParams, font_manager
from scipy.stats import linregress
from matplotlib.ticker import FuncFormatter
from matplotlib import colors
# To specify my own list of colours for matshow
from matplotlib.colors import ListedColormap
#To be able to use Latex encoding
rc('text', usetex = True)

# Ggplot = labels in dimgrey, among other things
plt.style.use('ggplot')


import sys

#text font easier to read
rcParams['text.latex.preamble'] = [r'\usepackage{sfmath}'] # \boldmath'


def createIrregularGradient(nb):
    """
    Create colorbar for values not well distributed
    (most are at the beginning  but the max is way higher / median >> mean)
    Takes a known colorbar and adds some chosen colors at the end
    """

    lColorNames = ["darkgreen","forestgreen","yellowgreen"] 
    lRGB = [colors.hex2color(colors.cnames[name]) for name in lColorNames]
    lRGB.extend(plt.cm.autumn_r(np.linspace(0, 1, nb)))
    return colors.LinearSegmentedColormap.from_list("mine", lRGB, 256)


def fit_line1(x, y):
    """Return parameters of the linear regression."""
    # Remove entries where either x or y is NaN.
    clean_data = pd.concat([x, y], 1).dropna(0) # row-wise
    (_, x), (_, y) = clean_data.iteritems()
    slope, intercept, r, p, stderr = linregress(x, y)
    return r**2, intercept, slope # could also return stderr


def plot_K():
    """
    Spatial map representing the local impact of increased mortality compared to reference scenario
    (log scale) N/Nmaxref
    N being the sum of Teneral and Female Adults
    Nmaxref is the map at the end of the reference scenario
    Function called from fig5
    """
    fig = plt.figure()
    K = np.genfromtxt("../input/environment/K30.txt")
    contour_levels = np.linspace(6.6,6550,100, endpoint = True)
    imdata = np.flipud(K)
    im = plt.contourf(imdata, cmap= createIrregularGradient(30) ,levels = contour_levels, interpolation = 'none') 
    plt.contour(imdata, np.linspace(6.6,6550,7), colors='black', alpha=0.7) 
    ax = plt.gca()
    ax.set_aspect('equal')
    major_yticks = np.arange(9,30,10)
    minor_yticks = np.arange(0,30,1)
    ax.set_yticks(major_yticks)
    ax.set_yticks(minor_yticks, minor = True)
    ax.set_xticks(major_yticks)
    ax.set_xticks(minor_yticks, minor = True)
    ax.set_xticklabels(['']*30)
    ax.set_yticklabels(['']*30)
    ax.tick_params(axis=u'both', which = 'minor', length = 5)
    ax.tick_params(axis=u'both', which = 'major', length = 10)

    cbar_ax = fig.add_axes([-0.01,0.1,0.05,0.8])

    cbar = fig.colorbar(im, ticks = np.arange(500,6501,1000), cax = cbar_ax) 
    cbar.ax.set_xticklabels(np.arange(6.6,6550,1000))
    cbar.ax.tick_params(axis = 'y', direction = "inout")
    plt.savefig("./fig_final/K.png", bbox_inches="tight")

def plot_contribution(fig, ax, name, bar, cbar_ax = None):
    """
    Spatial map representing the contribution of each cell to the total N/sum(N)
    N being the sum of Teneral and Female Adults
    Function called from fig5
    """
    N = np.genfromtxt(name)
    res = N/np.sum(N)
    contour_levels = np.linspace(0,0.018,100, endpoint = True)
    imdata = np.flipud(res)
    im = ax.contourf(imdata, cmap= createIrregularGradient(10), interpolation = 'none',levels = contour_levels)
    cont = ax.contour(imdata, np.linspace(0,0.018,5), colors='black', alpha=0.7)

    ax.set_aspect('equal')
    ax.set_axis_off()

    if bar == True:
        cbar = fig.colorbar(im, cax = cbar_ax, ticks = np.arange(-0.003,0.018,0.004))
        cbar.ax.tick_params(axis = 'y', direction = "inout")

def plot_local_impact(fig, ax, name, Nmaxref, bar, mask, suffix, cbar_ax = None):
    """
    Spatial map representing the local impact of increased mortality compared to reference scenario
    (log scale) N/Nmaxref
    N being the sum of Teneral and Female Adults
    Nmaxref is the map at the end of the reference scenario
    Function called from fig5
    """

    N = np.genfromtxt(name)
    res = np.log10(N/Nmaxref)

    mask = np.ma.masked_where((mask==1),mask)
    C = ListedColormap(["cyan","white"])

    if bar == "C2":
        im = ax.matshow(res, cmap = plt.cm.Purples_r, vmin = -2.2, vmax = -1.5) 
        cbar = fig.colorbar(im, cax = cbar_ax, ticks = np.arange(-2.2, -1.6, 0.3), orientation = "horizontal")
        cbar.ax.tick_params(labelrotation = 45, labelsize = 17)
    if bar == "C3":
        im = ax.matshow(res, cmap = plt.cm.Purples_r, vmin = -4.35, vmax = 0) 
        cbar = fig.colorbar(im, cax = cbar_ax, ticks = np.arange(-4, 0.5, 1), orientation = "horizontal")
        cbar.ax.tick_params(labelrotation = 45, labelsize = 16)

    ax.matshow(mask, cmap = C, alpha = 0.5)
    ax.set_aspect('equal')
    ax.set_axis_off()

def figS6(popFile, tempDirectory, duration, suffix = ""):
    """
    Panel of 4 figures describing the reference scenario
    The mean daily temperature, the number of individuals over time (space aggregated)
    for 3 years for P, T and F1:4+
    Comparison of age structure of the reference scenario and the data, by month
    Ratios plotted are F1,2,3/(F1+F2+F3)
    """

    fig = plt.figure()
    fig.set_size_inches(30,10) 
    outer_grid = gs.GridSpec(1,2, width_ratios = [1,2])
    X = np.linspace(0,duration,duration*365)
####################### TEMPERATURE AND REFERENCE SCENARIO ################
    left_part = outer_grid[0,0]
    inner_grid1 = gs.GridSpecFromSubplotSpec(2,1,left_part, height_ratios=[1,4])

### TEMPERATURE ###
    ax = plt.subplot(inner_grid1[0])
    ax.text(-0.11,1,r"\textbf{A}", transform=ax.transAxes, fontsize=28)
    mini = []
    maxi = []
    mean = []

#    avg_ecart_moy = []
#    avg_ecart_moy_abs = []
#    neg_ecart_moy = []
#    pos_ecart_moy = []
# Read the temperature files and record min/max/mean by day
# Stats to justify +-0.3°C sensitivity analysis
    for i in range(1,366):
        f = np.genfromtxt(tempDirectory+str(i)+".txt")

#        ecart_moyenne = f-np.mean(f)
#        ecart_abs_moyenne = np.abs(f-np.mean(f))

#        avg_ecart_moy.append(ecart_moyenne)
#        avg_ecart_moy_abs.append(ecart_abs_moyenne)

#        ecart_plus = ma.masked_where(ecart_moyenne<0, ecart_moyenne)
#        ecart_moins = ma.masked_where(ecart_moyenne>=0, ecart_moyenne)

#        neg_ecart_moy.append(ecart_moins)
#        pos_ecart_moy.append(ecart_plus)

        mini.append(np.min(f))
        maxi.append(np.max(f))
        mean.append(np.mean(f))

#    print np.max(ma.mean(neg_ecart_moy, axis=0)), np.max(ma.mean(pos_ecart_moy, axis=0))
#    print np.min(ma.mean(neg_ecart_moy, axis=0)), np.min(ma.mean(pos_ecart_moy, axis=0))
#    print np.mean(ma.mean(neg_ecart_moy, axis=0)), np.mean(ma.mean(pos_ecart_moy, axis=0))

    temp = [mean]*duration
# Transform list of list into list
    temp = [item for sublist in temp for item in sublist]
    ax.plot(X,temp, color ='dimgrey', lw=0.75)
    ax.set_ylim(bottom=18, top=30)

    ax.axes.get_xaxis().set_visible(False)

    ax.set_facecolor("white")

    major_yticks = np.arange(18,32,4)
    minor_yticks = np.arange(18,32,2)
    ax.set_yticks(major_yticks)
    ax.set_yticks(minor_yticks, minor = True)

    major_xticks = np.arange(0,3.1,1)
    minor_xticks = np.arange(0,3.1,.25)
    ax.set_xticks(major_xticks)
    ax.set_xticks(minor_xticks, minor = True)

    ax.tick_params('both', length=6, width=2, which='major')
    ax.tick_params('both', length=3, width=1, which='minor')

    ax.set_ylabel(r"$\theta_t (^\circ C)$")
    ax.set_xlim(0,3)

#Frame around the plot
    ax.grid = False
    for spine in ['left','right','top','bottom']:
        ax.spines[spine].set_color('k')

    for i in range(0, duration+1, 2): #xrange (python2)
        plt.axvspan(i, i+1, facecolor="gray", alpha=0.3, linewidth=0)

### REFERENCE SCENARIO ###
    pupColor = "indianred"
    femaleColor = "crimson"
    teneralColor = "darkred"

    ax = plt.subplot(inner_grid1[1])
    ax.text(-0.11,1,r"\textbf{B}", transform=ax.transAxes, fontsize=28)
    df = pd.read_csv(popFile, sep=" ")
    df = df[-1095:]
    df["A"] = df[["ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
    ax.plot(X, df["PUP"], color = pupColor, lw=3)
    ax.plot(X, df["TENERAL"], color = teneralColor, lw=3)
    ax.plot(X, df["A"], color = femaleColor, lw=3)

    ax.set_xlim(0,3)


    ax.text(0.34, 0.75, r"$P$", transform=ax.transAxes, fontsize = 27, color = pupColor)
    ax.text(0.34, 0.23, r"$F_{1:4+}$", transform=ax.transAxes, fontsize=27, color = femaleColor) # $\sum\limits_{x = 1}^{4+}F_x$
    ax.text(0.34, 0.09, r"$N$", transform=ax.transAxes, fontsize=27, color = teneralColor)

    ax.set_ylabel("Number of individuals", fontsize = 35)
    ax.set_xlabel("Time (years)", fontsize = 35)
    ax.set_facecolor("white")

    major_xticks = np.arange(0,3.1,1)
    minor_xticks = np.arange(0,3.1,.25)
    ax.set_xticks(major_xticks)
    ax.set_xticks(minor_xticks, minor = True)

    major_yticks = np.arange(0,9.1*10**5,1*10**5)
    minor_yticks = np.arange(0,9.1*10**5,0.5*10**5)
    ax.set_yticks(major_yticks)
    ax.set_yticks(minor_yticks, minor = True)

    ax.tick_params('both', length=7, width=2, which='major')
    ax.tick_params('both', length=4.5, width=1, which='minor')

    for spine in ['left','right','top','bottom']:
        ax.spines[spine].set_color('k')

# Every other year gray background
    for i in range(0, duration+1, 2): #xrange (python2)
        plt.axvspan(i, i+1, facecolor="gray", alpha=0.3, linewidth=0)

#Scientific notation
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

######################## AGE STRUCTURE COMPARISON #################
    right_part = outer_grid[0,1]
    inner_grid2 = gs.GridSpecFromSubplotSpec(1,2,right_part)
    ax1 = plt.subplot(inner_grid2[0,0])
    ax1.text(-0.16,1,r"\textbf{C}", transform=ax1.transAxes, fontsize=28)
    ax2 = plt.subplot(inner_grid2[0,1])
    ax2.text(-0.12,1,r"\textbf{D}", transform=ax2.transAxes, fontsize=28)


    F1Color = "#E69F00"
    F2Color = "#009E73"
    F3Color = "#56B4E9"

### REFERENCE SCENARIO ####
# File generated by age_structure_comparison/age_structure_formatting.R
    age = pd.read_csv("./age_structure_comparison/ref_pyramide_month_stat"+suffix+".csv", sep = " ")
    month_list = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    nb_days_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    student_quantile = [1.697,1.703,1.697,1.699,1.697,1.699,1.697,1.697,1.699,1.697,1.699,1.697]
    ax1.set_ylabel("Age structure", fontsize = 35)
    ax1.set_ylim([0,1])
    ax1.set_xlim([0,11])

    age["ICminA1"] = age["meanA1"] - student_quantile * age["varA1"]/np.sqrt(nb_days_list)
    age["ICmaxA1"] = age["meanA1"] + student_quantile * age["varA1"]/np.sqrt(nb_days_list)

    age["ICminA2"] = (age["meanA1"] + age["meanA2"]) - student_quantile * age["varA2"]/np.sqrt(nb_days_list)
    age["ICmaxA2"] = (age["meanA1"] + age["meanA2"]) + student_quantile * age["varA2"]/np.sqrt(nb_days_list)

    ax1.stackplot(np.arange(12),[age["meanA1"],age["meanA2"],age["meanA3"]], colors = [F1Color, F2Color, F3Color])

    ax1.fill_between(np.arange(12),age["ICminA1"], age["ICmaxA1"], color = "black", alpha = 0.5)
    ax1.fill_between(np.arange(12),age["ICminA2"], age["ICmaxA2"], color = "black", alpha = 0.5)


    ax1.tick_params('both', length=7, width=2, which='major')
    ax1.tick_params('both', length=4.5, width=1, which='minor')
    ax1.set_xticks(np.arange(0,12,2))
    ax1.set_xticks(np.arange(0,12,1), minor= True)
    ax1.set_yticks(np.arange(0,1,0.1), minor= True)

    labels = [item.get_text() for item in ax1.get_xticklabels()]
    labels = month_list[::2]
    ax1.set_xticklabels(labels, rotation = 45)

    ax1.text(0.5, 0.85, r"$F_3$", transform=ax1.transAxes, fontsize = 30)
    ax1.text(0.5, 0.52, r"$F_2$", transform=ax1.transAxes, fontsize = 30)
    ax1.text(0.5, 0.15, r"$F_1$", transform=ax1.transAxes, fontsize = 30)


######### DATA ##############

    age = pd.read_csv("./age_structure_comparison/field_pyramide_month_stat_all.csv", sep = " ")
    month_list = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

    nb_cap_list = [3,3,10,6,3,12,3,3,9,3,3,12] # nb of captures per month (aggregated, all traps and years)
    student_quantile = [2.920,2.920,1.833,2.015,2.920,1.796,2.920,2.920,1.860,2.920,2.920,1.796]


    ax2.set_ylabel("")
    ax2.set_ylim([0,1])
    ax2.tick_params(labelleft='off')

    age["ICminA1"] = age["meanA1"] - student_quantile * age["varA1"]/np.sqrt(nb_cap_list)
    age["ICmaxA1"] = age["meanA1"] + student_quantile * age["varA1"]/np.sqrt(nb_cap_list)

    age["ICminA2"] = (age["meanA1"] + age["meanA2"]) - student_quantile * age["varA2"]/np.sqrt(nb_cap_list)
    age["ICmaxA2"] = (age["meanA1"] + age["meanA2"]) + student_quantile * age["varA2"]/np.sqrt(nb_cap_list)

    ax2.stackplot(np.arange(12),[age["meanA1"],age["meanA2"],age["meanA3"]], colors = [F1Color, F2Color, F3Color])

    ax2.fill_between(np.arange(12),age["ICminA1"], age["ICmaxA1"], color = "black", alpha = 0.4)
    ax2.fill_between(np.arange(12),age["ICminA2"], age["ICmaxA2"], color = "black", alpha = 0.4)


    ax2.set_xlim([0,11])
    ax2.tick_params('both', length=7, width=2, which='major')
    ax2.tick_params('both', length=4.5, width=1, which='minor')
    ax2.set_xticks(np.arange(0,12,2))
    ax2.set_xticks(np.arange(0,12,1), minor= True)
    ax2.set_yticks(np.arange(0,1,0.1), minor= True)

    labels = [item.get_text() for item in ax2.get_xticklabels()]
    labels = month_list[::2]
    ax2.set_xticklabels(labels, rotation = 45)

    ax2.text(0.5, 0.85, r"$F_3$", transform=ax2.transAxes, fontsize = 30)
    ax2.text(0.5, 0.52, r"$F_2$", transform=ax2.transAxes, fontsize = 30)
    ax2.text(0.5, 0.15, r"$F_1$", transform=ax2.transAxes, fontsize = 30)

    plt.setp(ax.get_xticklabels(), fontsize = 35)
    plt.setp(ax1.get_xticklabels(), fontsize = 35)
    plt.setp(ax2.get_xticklabels(), fontsize = 35)

    plt.setp(ax.get_yticklabels(), fontsize = 35)
    plt.setp(ax1.get_yticklabels(), fontsize = 35)

    fig.subplots_adjust(wspace=.15)

    plt.savefig("./fig_revised/fig_S6_revised"+suffix+".png", bbox_inches = "tight")

def figS9(suffix = ""):
    """
    Panel of two figures (black and white) on sensitivity analyses
    First figure compares the effect of carrying capacity and temperature on population size, +-5%
    Second figure is the result of the FAST, first order effect and interactions on population size
    Population size : Teneral + Female Adults
    """

    fig = plt.figure()
    fig.set_size_inches(10,10)
    grid = gs.GridSpec(1,1)

########## TEMP/K EFFECT #########
    ax1 = plt.subplot(grid[0])
    ax1.text(0.05,0.94,"A", transform=ax1.transAxes, fontsize=23)

    ref = pd.read_csv("../outputs_paper/ref_3y"+suffix+"/time/pop.txt", sep=" ")
    ref = ref.iloc[0:1096,:] # First 3 years only
    lT = np.linspace(0,3,ref.shape[0])
    ref["A"] = ref[["ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
    ref["nonSF"] = ref[["A","TENERAL"]].sum(axis=1)

    path="../outputs_paper/influence_k_T/"

    Tplus = pd.read_csv(path+"Tplus"+suffix+"/time/pop.txt", sep=" ")
    Tplus["A"] = Tplus[["ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
    Tplus["nonSF"] = Tplus[["A","TENERAL"]].sum(axis=1)

    Tmoins = pd.read_csv(path+"Tmoins"+suffix+"/time/pop.txt", sep=" ")
    Tmoins["A"] = Tmoins[["ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
    Tmoins["nonSF"] = Tmoins[["A","TENERAL"]].sum(axis=1)

    Kplus = pd.read_csv(path+"Kplus"+suffix+"/time/pop.txt", sep=" ")
    Kplus["A"] = Kplus[["ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
    Kplus["nonSF"] = Kplus[["A","TENERAL"]].sum(axis=1)

    Kmoins = pd.read_csv(path+"Kmoins"+suffix+"/time/pop.txt", sep=" ")
    Kmoins["A"] = Kmoins[["ADULT1", "ADULT2", "ADULT3", "ADULT4"]].sum(axis=1)
    Kmoins["nonSF"] = Kmoins[["A","TENERAL"]].sum(axis=1)


    ax1.plot(lT, ref["nonSF"], label="Reference", color="black")    #yellow in sce
    ax1.plot(lT, Tplus["nonSF"], label=r"Temperature $\pm$5\%", color="lightslategrey", lw=2, ls='dashdot') #, ls='dashdot'
    ax1.plot(lT, Tmoins["nonSF"], color="lightslategrey", lw=2, ls = 'dashed') #, ls = 'dashed'
    ax1.fill_between(lT, Kplus["nonSF"], Kmoins["nonSF"], facecolor="dimgrey", alpha=0.3)

    ax1.set_xlabel("Time (years)")
    ax1.set_ylabel(r"Number of females (N+F$_{1:4+}$)")
    ax1.set_facecolor('white')

    ax1.set_xlim(0,3)
    ax1.set_ylim(0,5.9)

    major_xticks = np.arange(0,3.1,1)
    minor_xticks = np.arange(0,3.1,.25)
    ax1.set_xticks(major_xticks)
    ax1.set_xticks(minor_xticks, minor = True)

    major_yticks = np.arange(0,5.6*10**5,1*10**5)
    minor_yticks = np.arange(0,5.6*10**5,0.5*10**5)
    ax1.set_yticks(major_yticks)
    ax1.set_yticks(minor_yticks, minor = True)

    ax1.tick_params('both', length=5, width=2, which='major')
    ax1.tick_params('both', length=4.5, width=1, which='minor')

    for spine in ['left','right','top','bottom']:
        ax1.spines[spine].set_color('k')

    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    ax1.text(0.33, 0.85, r"$\theta + 5\%$", transform=ax1.transAxes, fontsize = 28, color="lightslategrey")
    ax1.text(0.5, 0.15, r"$\theta - 5\%$", transform=ax1.transAxes, fontsize = 28, color="lightslategrey")
    ax1.text(0.65, 0.28, r"$k \pm 5\%$", transform=ax1.transAxes, fontsize = 28, color="dimgrey")
    ax1.text(0.43, 0.35, r"ref", transform=ax1.transAxes, fontsize = 28, color="black")

    plt.savefig("fig_revised/fig_S9_left_revised"+suffix+".png", bbox_inches = "tight")


def figS12_control_homo(tempDirectory, suffix = ""):
    """
    We take the scenario with mortality +60% compared to reference and look at the local impact in cells
    depending on their properties
    k has no impact while mean temperature and its standard deviation have an effect
    the map of standard deviation of temperatures seem similar to the plots shown in fig5 bottom left
    """
    T = []
    for i in range(1,366):
        f = np.genfromtxt(tempDirectory+str(i)+".txt")
        T.append(f)

    mean_temp = np.mean(T, axis = 0)
    var_temp = np.std(T, axis = 0)


    nameRef = "../outputs_paper/ref_1y"+suffix+"/space/map_day365.txt"
    NmaxRef = np.genfromtxt(nameRef)

    name60 = "../outputs_paper/optimization_control/pixel100_duration365_obj2"+suffix+"/space/map_day365.txt"
    N = np.genfromtxt(name60)

    res8 = np.log10(N/NmaxRef)

    K = np.genfromtxt("../input/environment/K30.txt")

    fig = plt.figure()
    fig.set_size_inches(13,10)
    grid = gs.GridSpec(2,2)
    grid.update(hspace = 0.25)
    cmap_choice = "winter_r"

    ax1 = plt.subplot(grid[0,0]) # K
    ax2 = plt.subplot(grid[0,1]) # varTemp spatial
    ax3 = plt.subplot(grid[1,0]) # meanTemp
    ax4 = plt.subplot(grid[1,1]) # varTemp

    ax1.scatter(K.flat, res8.flat, c = var_temp.flat, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha = 0.7)
    ax1.set_xlim([-0.5,np.max(K)])

    for ax in [ax1, ax3, ax4]:
        ax.set_facecolor("white")
        ax.tick_params('both', length=4, width=2, which='major')
        ax.tick_params('both', length=3, width=1, which='minor')
        ax.set_ylim([np.min(res8), np.max(res8)])#([-2.17 , -1.53])
        major_yticks = np.arange(-2.2,-1.5,0.2)
        minor_yticks = np.arange(-2.2,-1.5,0.1)
        ax.set_yticks(major_yticks)
        ax.set_yticks(minor_yticks, minor = True)
        for spine in ['left','right','top','bottom']:
            ax.spines[spine].set_color('dimgrey')

    major_xticks1 = np.arange(0,7000,1500)
    minor_xticks1 = np.arange(0,7000,750)
    ax1.set_xticks(major_xticks1)
    ax1.set_xticks(minor_xticks1, minor = True)

    ax1.set_xlabel(r"Carrying capacity $k_c$")

    contour_levels = np.linspace(np.min(var_temp),np.max(var_temp),100, endpoint = True)

    imdata = np.flipud(var_temp)
    im = ax2.contourf(imdata, cmap= cmap_choice, interpolation = 'none',levels = contour_levels) # norm = norm
    cont = ax2.contour(imdata, colors='black', alpha=0.6)
    lab = ax2.clabel(cont, fmt='%1.2f', fontsize=12, alpha = 1)

    ax2.set_xticklabels([''])
    ax2.set_yticklabels([''])
    ax2.tick_params(axis=u'both', which = u'both', length = 0)
    ax2.set_aspect('equal')
    ax2.set_xlabel("Temperature variation in the study area")
    cbar_ax2 = fig.add_axes([0.93,0.54,0.02,0.36])
    cbar2 = fig.colorbar(im, ticks = np.arange(1.95,2.35,0.05), cax = cbar_ax2)
    cbar2.ax.tick_params(axis = "y", direction = "inout")

    ax3.scatter(mean_temp.flat, res8.flat, c = var_temp.flat, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha = 0.7)
    ax3.set_xlabel(r"Mean annual temperature ($^\circ$C)")
    ax3.set_xlim([np.min(mean_temp), np.max(mean_temp)])

    minor_xticks3 = np.arange(23.7,24.3,0.1)
    ax3.set_xticks(minor_xticks3, minor = True)
    major_xticks3 = np.arange(23.7,24.3,0.2)
    ax3.set_xticks(major_xticks3)

    common_ax_text1 = r"Local relative population decrease"
    common_ax_text2 = r" after \textbf{homogeneous} control (log10 scale)"

    txt1 = ax3.text(-0.26, 1.05, common_ax_text1, transform=ax3.transAxes, fontsize=22,
     rotation = 90, color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center")

    txt2 = ax3.text(-0.2, 1.05, common_ax_text2, transform=ax3.transAxes, fontsize=22,
     rotation = 90, color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center")

    sc4 = ax4.scatter(var_temp.flat, res8.flat, c = mean_temp.flat, cmap = "seismic", edgecolor = "none", s = 50, marker = '^')
    cbar_ax4 = fig.add_axes([0.93,0.1,0.02,0.36])
    cbar4 = plt.colorbar(sc4, cax = cbar_ax4)
    cbar4.ax.tick_params(axis = "y", direction = "inout")
    ax4.set_xlabel(r"Annual standard deviation ($^\circ$C)")
    minor_xticks4 = np.arange(2,2.4,0.05)
    ax4.set_xticks(minor_xticks4, minor = True)


    slope, intercept, r, p, stderr = linregress(K.flatten(), res8.flatten())
#    print(r**2, slope, intercept, p, stderr)
    ax1.text(-0.07, 0.93, "A", transform = ax1.transAxes, fontsize = 18)
    ax1.text(0.75,0.35, r"$r^2$ = {:.2f}".format(r**2), transform = ax1.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")

    ax2.text(-0.09, 0.93, "B", transform = ax2.transAxes, fontsize = 18)

    slope, intercept, r, p, stderr = linregress(mean_temp.flatten(), res8.flatten())
#    print(r**2, slope, intercept, p, stderr)
    ax3.text(-0.07, 0.93, "C", transform = ax3.transAxes, fontsize = 18)
    ax3.text(0.79,0.53, r"$r^2$ = {:.2f}".format(r**2), transform = ax3.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")


    slope, intercept, r, p, stderr = linregress(var_temp.flatten(), res8.flatten())
#    print(r**2, slope, intercept, p, stderr)
    ax4.text(-0.07, 0.93, "D", transform = ax4.transAxes, fontsize = 18)
    ax4.text(0.83,0.57, r"$r^2$ = {:.2f}".format(r**2), transform = ax4.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")

    plt.savefig("./fig_revised/fig_S12_control_homo"+suffix+".png", bbox_inches="tight", bbox_extra_artists = (cbar_ax2, cbar_ax4, txt1, txt2))


def figS13_control_hetero(tempDirectory, suffix = ""):
    """
    We take the scenario with mortality +60% compared to reference and look at the local impact in cells
    depending on their properties
    k has no impact while mean temperature and its standard deviation have an effect
    the map of standard deviation of temperatures seem similar to the plots shown in fig5 bottom left
    """
    T = []
    for i in range(1,366):
        f = np.genfromtxt(tempDirectory+str(i)+".txt")
        T.append(f)

    mean_temp = np.mean(T, axis = 0)
    var_temp = np.std(T, axis = 0)
    K = np.genfromtxt("../input/environment/K30.txt")


    nameRef = "../outputs_paper/ref_1y"+suffix+"/space/map_day365.txt"
    NmaxRef = np.genfromtxt(nameRef)

    if suffix == "":
        max_pxl_hetero = 47
    else:
        max_pxl_hetero = 51
    name60 = "../outputs_paper/optimization_control/pixel"+str(max_pxl_hetero)+"_duration365_obj2"+suffix+"/space/map_day365.txt"
    N = np.genfromtxt(name60)
    res8 = np.log10(N/NmaxRef)
    mask = np.genfromtxt("../outputs_paper/optimization_control/pixel"+str(max_pxl_hetero)+"_duration365_obj2"+suffix+"/space/mask.txt")

    M = []
    C = []
    K_correl = []
    mean_temp_correl = []
    var_temp_correl = []
    res_correl = []

    Kf = K.flat
    MTf = mean_temp.flat
    VTf = var_temp.flat
    Rf = res8.flat
    Mf = mask.flat
    for i in range(len(Mf)):
        if Mf[i] == 1:
            M.append(".")
            C.append("orange")
            K_correl.append(Kf[i])
            mean_temp_correl.append(MTf[i])
            var_temp_correl.append(VTf[i])
            res_correl.append(Rf[i])
        else:
            M.append("x")
            C.append("cyan")

    fig = plt.figure()
    fig.set_size_inches(30,10)
    grid = gs.GridSpec(1,3)
    grid.update(hspace = 0.25)
    cmap_choice = "winter_r"

    ax1 = plt.subplot(grid[0,0]) # K
    ax3 = plt.subplot(grid[0,1]) # meanTemp 1,0
    ax4 = plt.subplot(grid[0,2]) # varTemp 1,1

    ax1.scatter(Kf, Rf, color = C, s=50, marker= '^', edgecolor = "none", alpha = 0.7)
    ax1.set_xlim([-0.5,np.max(K)])

    for ax in [ax1, ax3, ax4]:
        ax.set_facecolor("white")
        ax.tick_params('both', length=4, width=2, which='major')
        ax.tick_params('both', length=3, width=1, which='minor')
        ax.set_ylim([np.min(res8), np.max(res8)+0.1])#([-3.77,0])
        major_yticks = np.arange(-3.8,0.1,1.)
        minor_yticks = np.arange(-3.8,0.1,0.5)
        ax.set_yticks(major_yticks)
        ax.set_yticks(minor_yticks, minor = True)
        for spine in ['left','right','top','bottom']:
            ax.spines[spine].set_color('dimgrey')

    major_xticks1 = np.arange(0,7000,1500)
    minor_xticks1 = np.arange(0,7000,750)
    ax1.set_xticks(major_xticks1)
    ax1.set_xticks(minor_xticks1, minor = True)

    ax1.set_xlabel(r"Carrying capacity $k_c$")

    ax3.scatter(MTf, Rf, color = C, s=50, marker= '^', edgecolor = "none", alpha = 0.7) 
    ax3.set_xlabel(r"Mean annual temperature ($^\circ$C)")
    ax3.set_xlim([np.min(mean_temp), np.max(mean_temp)])

    minor_xticks3 = np.arange(23.7,24.3,0.1)
    ax3.set_xticks(minor_xticks3, minor = True)
    major_xticks3 = np.arange(23.7,24.3,0.2)
    ax3.set_xticks(major_xticks3)

    common_ax_text1 = r"Local relative population decrease"
    common_ax_text2 = r" after \textbf{heterogeneous} control (log10 scale)"

    txt1 = ax1.text(-0.2, 0.5, common_ax_text1, transform=ax1.transAxes, fontsize=24,
     rotation = 90, color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center")

    txt2 = ax1.text(-0.14, 0.5, common_ax_text2, transform=ax1.transAxes, fontsize=24,
     rotation = 90, color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center")

    sc4 = ax4.scatter(VTf, Rf, color = C, edgecolor = "none", s = 50, marker = '^') 
    ax4.set_xlabel(r"Annual standard deviation ($^\circ$C)")
    minor_xticks4 = np.arange(2,2.4,0.05)
    ax4.set_xticks(minor_xticks4, minor = True)


    slope, intercept, r, p, stderr = linregress(K_correl, res_correl) 
#    print(r**2, slope, intercept, p, stderr)
    ax1.text(-0.07, 0.93, "A", transform = ax1.transAxes, fontsize = 22)
    ax1.text(0.9,0.35, r"$r^2$ = {:.2f}".format(r**2), transform = ax1.transAxes, fontsize = 22,
    horizontalalignment = "center", verticalalignment = "center")

    ax1.text(0.25,0.91, "cells without control", transform = ax1.transAxes, fontsize = 22,
    horizontalalignment = "center", verticalalignment = "center", color = "cyan")

    ax1.text(0.25,0.45, "cells with control", transform = ax1.transAxes, fontsize = 22,
    horizontalalignment = "center", verticalalignment = "center", color = "orange")

    slope, intercept, r, p, stderr = linregress(mean_temp_correl, res_correl)
#    print(r**2, slope, intercept, p, stderr)
    ax3.text(-0.07, 0.93, "B", transform = ax3.transAxes, fontsize = 22)
    ax3.text(0.9,0.35, r"$r^2$ = {:.2f}".format(r**2), transform = ax3.transAxes, fontsize = 22,
    horizontalalignment = "center", verticalalignment = "center")


    slope, intercept, r, p, stderr = linregress(var_temp_correl, res_correl)
#    print(r**2, slope, intercept, p, stderr)
    ax4.text(-0.07, 0.93, "C", transform = ax4.transAxes, fontsize = 22)
    ax4.text(0.9,0.35, r"$r^2$ = {:.2f}".format(r**2), transform = ax4.transAxes, fontsize = 22,
    horizontalalignment = "center", verticalalignment = "center")

    plt.savefig("./fig_revised/figS13_control_hetero"+suffix+".png", bbox_inches="tight", bbox_extra_artists = (txt1, txt2)) #cbar_ax2, cbar_ax4

def fig4_resurgence_hetero(tempDirectory, suffix=""):
    """
    We take the scenario with mortality +60% compared to reference and look at the local impact in cells
    depending on their properties
    k has no impact while mean temperature and its standard deviation have an effect
    the map of standard deviation of temperatures seem similar to the plots shown in fig5 bottom left
    """
    T = []
    for i in range(1,366):
        f = np.genfromtxt(tempDirectory+str(i)+".txt")
        T.append(f)

    mean_temp = np.mean(T, axis = 0)
    var_temp = np.std(T, axis = 0)

    if suffix == "":
        max_pxl_hetero = 47
    else:
        max_pxl_hetero = 51

    nameRef = "../outputs_paper/optimization_control/pixel"+str(max_pxl_hetero)+"_duration365_obj2"+suffix+"/space/map_day365.txt"

    NmaxRef = np.genfromtxt(nameRef)
    name60 = "../outputs_paper/resurgence/resurgence_1y_pixel"+str(max_pxl_hetero)+"_obj2"+suffix+"/space/map_day365.txt"

    N = np.genfromtxt(name60)
    res8 = (N-NmaxRef)/NmaxRef 

    K = np.genfromtxt("../input/environment/K30.txt")

    mask = np.genfromtxt("../outputs_paper/optimization_control/pixel"+str(max_pxl_hetero)+"_duration365_obj2"+suffix+"/space/mask.txt")
    score = np.genfromtxt("../outputs_paper/optimization_control/pixel90_duration365_obj2"+suffix+"/space/Pij.txt")


    M = []
    C = []
    K_correl = []
    mean_temp_correl = []
    var_temp_correl = []
    res_correl = []
    score_correl = []

    Kf = K.flat
    MTf = mean_temp.flat
    VTf = var_temp.flat
    Rf = res8.flat
    Mf = mask.flat
    Sf = score.flat

    for i in range(len(Mf)):
        if Mf[i] == 1:
            M.append(".")
            C.append("white") #orange
            K_correl.append(Kf[i])
            mean_temp_correl.append(MTf[i])
            var_temp_correl.append(VTf[i])
            res_correl.append(Rf[i])
            score_correl.append(Sf[i])
        else:
            M.append("x")
            C.append("cyan") 

    fig = plt.figure()
    fig.set_size_inches(13,10)
    grid = gs.GridSpec(2,2)
    grid.update(hspace = 0.25)
    cmap_choice = "plasma" 

    ax1 = plt.subplot(grid[0,1]) # K
    ax2 = plt.subplot(grid[0,0]) # varTemp spatial
    ax3 = plt.subplot(grid[1,0]) # meanTemp
    ax4 = plt.subplot(grid[1,1]) # varTemp

    ax1.scatter(Kf, Rf, color = C, s=50, marker= '^', edgecolor = "none")
    ax1.scatter(K_correl, res_correl, c = score_correl, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha=0.6) #color = C

    ax1.set_xlim([-0.5,np.max(K)])

    for ax in [ax1, ax3, ax4]:
        ax.set_facecolor("white")
        ax.tick_params('both', length=4, width=2, which='major')
        ax.tick_params('both', length=3, width=1, which='minor')
        ax.set_ylim([np.min(res8), np.max(res8)])

        if suffix == "":
            major_yticks = np.arange(0,45,10)
            minor_yticks = np.arange(0,45,5)
        else:
            major_yticks = np.arange(-0.5,0.72,0.2)
            minor_yticks = np.arange(-0.5,0.72,0.1)

        ax.set_yticks(major_yticks)
        ax.set_yticks(minor_yticks, minor = True)

    major_xticks1 = np.arange(0,7000,1500)
    minor_xticks1 = np.arange(0,7000,750)
    ax1.set_xticks(major_xticks1)
    ax1.set_xticks(minor_xticks1, minor = True)

    ax1.set_xlabel(r"Carrying capacity $k_c$")

    if suffix == "":
        im = ax2.matshow(res8, cmap= createIrregularGradient(60)) 
    else:
        im = ax2.matshow(res8, cmap= createIrregularGradient(5))

    ax2.set_axis_off()
    ax2.set_aspect('equal')
    ax2.text(0.5,-0.15,"Resurgence speed in the study area",transform=ax2.transAxes, fontsize=22,
     color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center")
    cbar_ax2 = fig.add_axes([0.1,0.53,0.02,0.36]) 

    if suffix == "":
        cbar2 = fig.colorbar(im, ticks = np.arange(0,45,10), cax = cbar_ax2)
    else:
        cbar2 = fig.colorbar(im, ticks = np.arange(-0.4,0.7,0.2), cax = cbar_ax2)

    cbar2.ax.tick_params(axis = "y", direction = "inout")
    cbar_ax2.yaxis.set_ticks_position("left")

    ax3.scatter(MTf, Rf, color = C, s=50, marker= '^', edgecolor = "none")
    ax3.scatter(mean_temp_correl, res_correl, c = score_correl, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha = 0.6) #color = C

    ax3.set_xlabel(r"Mean annual temperature ($^\circ$C)")
    ax3.set_xlim([np.min(mean_temp), np.max(mean_temp)])

    minor_xticks3 = np.arange(23.7,24.3,0.1)
    ax3.set_xticks(minor_xticks3, minor = True)
    major_xticks3 = np.arange(23.7,24.3,0.2)
    ax3.set_xticks(major_xticks3)

    ax1.set_ylabel("Population variation")
    ax3.set_ylabel("Population variation")


    ax4.scatter(VTf, Rf, color = C, edgecolor = "none", s = 50, marker = '^')
    sc4 = ax4.scatter(var_temp_correl, res_correl, c = score_correl, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha = 0.6) #color = C

    cbar_ax4 = fig.add_axes([0.93,0.1,0.02,0.36])
    cbar4 = plt.colorbar(sc4, cax = cbar_ax4)
    cbar4.ax.tick_params(axis = "y", direction = "inout")

    ax4.set_xlabel(r"Annual standard deviation ($^\circ$C)")
    minor_xticks4 = np.arange(2,2.4,0.05)
    ax4.set_xticks(minor_xticks4, minor = True)


    slope, intercept, r, p, stderr = linregress(K_correl, res_correl)
#    print(r**2, slope, intercept, p, stderr)
    ax1.text(-0.15, 0.95, "B", transform = ax1.transAxes, fontsize = 18)
    ax1.text(0.55,0.25, r"$r^2$ = {:.2f}".format(r**2), transform = ax1.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")

    ax2.text(-0.15, 0.95, "A", transform = ax2.transAxes, fontsize = 18)

    slope, intercept, r, p, stderr = linregress(mean_temp_correl, res_correl)
#    print(r**2, slope, intercept, p, stderr)
    ax3.text(-0.15, 0.95, "C", transform = ax3.transAxes, fontsize = 18)
    ax3.text(0.85,0.6, r"$r^2$ = {:.2f}".format(r**2), transform = ax3.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")


    slope, intercept, r, p, stderr = linregress(var_temp_correl, res_correl)
#    print(r**2, slope, intercept, p, stderr)
    ax4.text(-0.15, 0.95, "D", transform = ax4.transAxes, fontsize = 18)
    ax4.text(0.85,0.6, r"$r^2$ = {:.2f}".format(r**2), transform = ax4.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")
    ax4.text(1.12,-0.16, "Control \nscore $P_j$".format(r**2), transform = ax4.transAxes, fontsize = 20,
    horizontalalignment = "center", verticalalignment = "center", color = "dimgrey")

    ax1.axhline(y=0, color="black", lw = 1.5) 

    ax3.axhline(y=0, color="black", lw = 1.5) 

    ax4.axhline(y=0, color="black", lw = 1.5) 

    ax1.text(0.53,0.94, "cells without control", transform = ax1.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center", color = "cyan") #purple

    if suffix == "":
        ax1.set_ylim(-1,45)
        ax3.set_ylim(-1,45)
        ax4.set_ylim(-1,45)
    else:
        ax1.set_ylim(-0.55,0.8)
        ax3.set_ylim(-0.55,0.8)
        ax4.set_ylim(-0.55,0.8)

    plt.savefig("./fig_revised/fig_4_resurg_hetero"+suffix+".tiff", bbox_inches="tight", bbox_extra_artists = (cbar_ax2,)) # txt1, txt2)) #, cbar_ax4

def fig3_resurgence_homo(tempDirectory, suffix = ""):
    """
    We take the scenario with mortality +60% compared to reference and look at the local impact in cells
    depending on their properties
    k has no impact while mean temperature and its standard deviation have an effect
    the map of standard deviation of temperatures seem similar to the plots shown in fig5 bottom left
    """
    T = []
    for i in range(1,366):
        f = np.genfromtxt(tempDirectory+str(i)+".txt")
        T.append(f)

    mean_temp = np.mean(T, axis = 0)
    var_temp = np.std(T, axis = 0)

    nameRef = "../outputs_paper/optimization_control/pixel100_duration365_obj2"+suffix+"/space/map_day365.txt" 

    NmaxRef = np.genfromtxt(nameRef)

    name60 = "../outputs_paper/resurgence/resurgence_1y_pixel100_obj2"+suffix+"/space/map_day365.txt"


    N = np.genfromtxt(name60)
    res8 = (N-NmaxRef)/NmaxRef 

    K = np.genfromtxt("../input/environment/K30.txt")

    Kf = K.flat
    MTf = mean_temp.flat
    VTf = var_temp.flat
    Rf = res8.flat

    fig = plt.figure()
    fig.set_size_inches(13,10)
    grid = gs.GridSpec(2,2)
    grid.update(hspace = 0.25)
    cmap_choice = "terrain" 

    ax1 = plt.subplot(grid[0,1]) # K
    ax2 = plt.subplot(grid[0,0]) # varTemp spatial
    ax3 = plt.subplot(grid[1,0]) # meanTemp
    ax4 = plt.subplot(grid[1,1]) # varTemp

##### IMPACT OF CONTROL ##########
    nameRefc = "../outputs_paper/ref_1y"+suffix+"/space/map_day365.txt"
    NmaxRefc = np.genfromtxt(nameRefc)
    nameC = "../outputs_paper/optimization_control/pixel100_duration365_obj2"+suffix+"/space/map_day365.txt"
    Nc = np.genfromtxt(nameC)
    resC = np.log10(Nc/NmaxRefc)

    ax1.scatter(Kf, Rf, c = resC.flat, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha = 0.7, vmin = -2.2, vmax = -1.5) # color = "dimgrey"
    ax1.set_xlim([-0.5,np.max(K)])

    for ax in [ax1, ax3, ax4]:
        ax.set_facecolor("white")
        ax.tick_params('both', length=4, width=2, which='major')
        ax.tick_params('both', length=3, width=1, which='minor')
        ax.set_ylim([-0.42, 0.75])

        major_yticks = np.arange(-0.4,0.71,0.2)
        minor_yticks = np.arange(-0.4,0.71,0.1)

        ax.set_yticks(major_yticks)
        ax.set_yticks(minor_yticks, minor = True)

    major_xticks1 = np.arange(0,7000,1500)
    minor_xticks1 = np.arange(0,7000,750)
    ax1.set_xticks(major_xticks1)
    ax1.set_xticks(minor_xticks1, minor = True)

    ax1.set_xlabel(r"Carrying capacity $k_c$")


    im = ax2.matshow(res8, cmap = plt.cm.Purples_r) 
    ax2.set_axis_off()
    ax2.set_aspect('equal')
    ax2.text(0.5,-0.15,"Resurgence speed in the study area",transform=ax2.transAxes, fontsize=22,
     color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center")
    cbar_ax2 = fig.add_axes([0.1,0.53,0.02,0.36])
    cbar2 = fig.colorbar(im, ticks = np.arange(-0.4,0.75,0.2), cax = cbar_ax2)

    cbar2.ax.tick_params(axis = "y", direction = "inout")
    cbar_ax2.yaxis.set_ticks_position("left")

    ax3.scatter(MTf, Rf, c = resC.flat, cmap = cmap_choice, s=50, marker= '^', edgecolor = "none", alpha = 0.7, vmin = -2.2, vmax = -1.5)
    ax3.set_xlabel(r"Mean annual temperature ($^\circ$C)")
    ax3.set_xlim([np.min(mean_temp), np.max(mean_temp)])

    minor_xticks3 = np.arange(23.7,24.3,0.1)
    ax3.set_xticks(minor_xticks3, minor = True)
    major_xticks3 = np.arange(23.7,24.3,0.2)
    ax3.set_xticks(major_xticks3)


    ax1.set_ylabel("Population variation")
    ax3.set_ylabel("Population variation")

    sc4 = ax4.scatter(VTf, Rf, c = resC.flat, cmap = cmap_choice, edgecolor = "none", s = 50, marker = '^', vmin = -2.2, vmax = -1.5)
    cbar_ax4 = fig.add_axes([0.93,0.1,0.02,0.36])
    cbar4 = fig.colorbar(sc4, ticks = np.arange(-2.1,-1.49,0.2), cax = cbar_ax4)

    cbar4.ax.tick_params(axis = "y", direction = "inout")
    ax4.set_xlabel(r"Annual standard deviation ($^\circ$C)")
    minor_xticks4 = np.arange(2,2.4,0.05)
    ax4.set_xticks(minor_xticks4, minor = True)


    slope, intercept, r, p, stderr = linregress(K.flatten(), res8.flatten())
#    print(r**2, slope, intercept, p, stderr)
    ax1.text(-0.07, 0.93, "B", transform = ax1.transAxes, fontsize = 18)
    ax1.text(0.75,0.45, r"$r^2$ = {:.2f}".format(r**2), transform = ax1.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")

    ax2.text(-0.09, 0.93, "A", transform = ax2.transAxes, fontsize = 18)

    slope, intercept, r, p, stderr = linregress(mean_temp.flatten(), res8.flatten())
#    print(r**2, slope, intercept, p, stderr)
    ax3.text(-0.07, 0.93, "C", transform = ax3.transAxes, fontsize = 18)
    ax3.text(0.81,0.53, r"$r^2$ = {:.2f}".format(r**2), transform = ax3.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")


    slope, intercept, r, p, stderr = linregress(var_temp.flatten(), res8.flatten())
#    print(r**2, slope, intercept, p, stderr)
    ax4.text(-0.07, 0.93, "D", transform = ax4.transAxes, fontsize = 18)
    ax4.text(0.84,0.53, r"$r^2$ = {:.2f}".format(r**2), transform = ax4.transAxes, fontsize = 18,
    horizontalalignment = "center", verticalalignment = "center")

    ax4.text(1.12,-0.15, "Control\n efficacy", transform = ax4.transAxes, fontsize = 20,
    horizontalalignment = "center", verticalalignment = "center", color = "dimgrey")

    ax1.axhline(y=0, color="black", lw = 1.5) 
    ax3.axhline(y=0, color="black", lw = 1.5) 
    ax4.axhline(y=0, color="black", lw = 1.5) 


    ax1.set_ylim(-0.43,0.75)
    ax3.set_ylim(-0.43,0.75)
    ax4.set_ylim(-0.43,0.75)

    plt.savefig("./fig_revised/fig_3_resurg_homo"+suffix+".tiff", bbox_inches="tight", bbox_extra_artists = (cbar_ax2,)) #, txt1, txt2)) #, cbar_ax4

def fig2(suffix = ""):
    """
    Big Panel ! Main figure shows the decrease in population (compared to reference scenario)
    for different levels of mortality (from +2.5% to +100%)
    Spatial outputs : top line, contribution of cells to the total population
    is correlated with the carrying capacity
    left : local impact of the mortality level compared to reference
    is correlated to temperature (mean and std, see fig6)
    """
    fig = plt.figure()
    fig.set_size_inches(15, 13)
    grid = gs.GridSpec(4, 5, height_ratios=[0.5, 3, 3, 3], width_ratios = [1.25, 3, 3, 3, 1])
    grid.update(left = -0.02, right = 0.98, top = 1., wspace = 0.55, hspace = 0.)

# Text explaining contribution plot
    ax0 = plt.subplot(grid[0,1:], frameon = False) #text contrib
    ax0.axes.get_xaxis().set_visible(False)
    ax0.axes.get_yaxis().set_visible(False)
    text1 = r"Contribution of cells to the total population"
    ax0.text(0.43, 0.35, text1 , transform=ax0.transAxes, color = "dimgrey", horizontalalignment = "center", fontsize = 30)

# Contribution plots
    ax1 = plt.subplot(grid[1,1]) # ref contrib
    ax2 = plt.subplot(grid[1,2]) # homo contrib
    ax3 = plt.subplot(grid[1,3]) #hetero contrib

# Text explaining local impact plots
    ax7 = plt.subplot(grid[2:,0], frameon = False) #text impact
    ax7.axes.get_xaxis().set_visible(False)
    ax7.axes.get_yaxis().set_visible(False)
    text71 = r"Local relative population decrease"
    text73 = r"(log10 scale)"
    ax7.text(0.95, 0.5, text71 , transform=ax7.transAxes,
     rotation = 90, color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center", fontsize = 30) 

    ax7.text(1.3, 0.5, text73 , transform=ax7.transAxes,
     rotation = 90, color = "dimgrey",
     horizontalalignment = "center",
     verticalalignment = "center", fontsize = 30)

# Local impact plots
    ax4 = plt.subplot(grid[2,1]) # M1 impact
    ax5 = plt.subplot(grid[3,1]) # M2 impact
    ax6 = plt.subplot(grid[2:,2:]) # mort_control

    c = np.linspace(0,1,13)

    # the mort_A factor applied to achieve the control target can be seen in the stat.txt file in each simulation folder
    obj2_x = [1.75,1.7625,1.775,1.8,1.85,1.9,1.95,2.05,2.15,2.35,2.65] #mort_A_min
    obj2_y = [100, 90, 80, 70, 60, 56, 54, 52, 50, 48, 47] # % pixel treated

    ax6.plot(obj2_x, obj2_y, '--o', color = "black", label = r"2\%", markersize = 10)
    
    if suffix == "":
        ax6.plot(1.75, 100,'--o', color="orange", markersize = 10)
        ax6.plot(2.65, 47,'--o', color="violet", markersize = 10)
        obj5_x = [1.5875,1.5875,1.59375,1.60625,1.61875,1.66875,1.70625,1.78125,1.98125,2.03,2.23,2.73]
        obj5_y = [100, 90, 80, 70, 60, 50, 45, 40, 35, 34, 33, 32]

        ax6.plot(obj5_x, obj5_y, '--^', color = "black", label = r"5\%", markersize = 10)
    else:
        obj2_x_no_mig = [1.725, 1.725, 1.7375, 1.7625, 1.8625, 1.9625, 2.0625, 2.0625, 2.1625, 2.3625] #mort_A_min
        obj2_y_no_mig = [100, 90, 80, 70, 60, 55, 54, 53, 52, 51] # % pixel treated
        ax6.plot(obj2_x_no_mig, obj2_y_no_mig, '--^', color = "black", label = r"2\% (no migration)", markersize = 10)
        ax6.plot(1.725, 100,'--^', color="orange", markersize = 10)
        ax6.plot(2.3625, 51,'--^', color="violet", markersize = 10)

    if suffix == "":
        ax6.text(0.18,0.93,r"\textbf{2}", color = "orange", transform = ax6.transAxes) 
        ax6.text(0.9,0.39,r"\textbf{3}", color = "violet", transform = ax6.transAxes) 
    else:
        ax6.text(0.1,0.93,r"\textbf{2}", color = "orange", transform = ax6.transAxes) 
        ax6.text(0.66,0.49,r"\textbf{3}", color = "violet", transform = ax6.transAxes) 

    ax1.text(-0.2,0.5,r"$B_1$", transform = ax1.transAxes, verticalalignment = 'center', color = "dimgrey") 
    ax2.text(-0.2,0.5,r"$B_2$", transform = ax2.transAxes, verticalalignment = 'center', color = "orange")
    ax3.text(-0.2,0.5,r"$B_3$", transform = ax3.transAxes, verticalalignment = 'center', color = "violet")
    ax4.text(-0.2,0.5,r"$C_2$", transform = ax4.transAxes, verticalalignment = 'center', color = "orange")
    ax5.text(-0.2,0.5,r"$C_3$", transform = ax5.transAxes, verticalalignment = 'center', color = "violet")

    text6 = "Objective\n(N+F$_{1:4+}$) = X\% of initial female population"
    ax6.text(0.67, 0.92, text6, transform = ax6.transAxes, verticalalignment = 'center', horizontalalignment = 'center', color = "dimgrey")
    ax6.text(-0.05, 1.02, "A", transform = ax6.transAxes, verticalalignment = 'center', horizontalalignment = 'center', color = "dimgrey")

    lgd = ax6.legend(bbox_transform = ax6.transAxes, bbox_to_anchor = (0.79, 0.87), fontsize = 18)
    for text in lgd.get_texts():
        text.set_color("dimgrey")
    frame = lgd.get_frame()
    frame.set_color("white")

    ax6.set_ylabel(r"\% of cells treated")
    ax6.set_xlabel("Relative increase in mortality needed\nto achieve objective after 1 year of control")

    ax6.set_facecolor("white")
    ax6.set_ylim(0,105)
    ax6.set_xlim(1.55,2.81)

    major_xticks = np.arange(1.6,2.81,0.5)
    minor_xticks = np.arange(1.6,2.81,0.25)
    ax6.set_xticks(major_xticks)
    ax6.set_xticks(minor_xticks, minor = True)

    major_yticks = np.arange(0,110,20)
    minor_yticks = np.arange(0,110,10)
    ax6.set_yticks(major_yticks)
    ax6.set_yticks(minor_yticks, minor = True)

    ax6.tick_params('both', length=4.5, width=2, which='major')
    ax6.tick_params('both', length=3.5, width=1, which='minor')

    for spine in ['left','right','top','bottom']:
        ax6.spines[spine].set_color('dimgrey')

# Place axes for colorbars
# width, length, pos x , pos y
    cb1 = fig.add_axes([0.89, 0.705, 0.02, 0.215])
    cb2 = fig.add_axes([0.132, 0.085, 0.185, 0.02])
    cb3 = fig.add_axes([0.132, 0.4, 0.185, 0.02])

    nameRef = "../outputs_paper/ref_1y"+suffix+"/space/map_day365.txt"
    name5 = "../outputs_paper/optimization_control/pixel100_duration365_obj2"+suffix+"/space/map_day365.txt"
    if suffix == "":
        max_pxl_hetero = 47
    else:
        max_pxl_hetero = 51
    name60 = "../outputs_paper/optimization_control/pixel"+str(max_pxl_hetero)+"_duration365_obj2"+suffix+"/space/map_day365.txt"
    Nmaxref = np.genfromtxt(nameRef)
    N5 = np.genfromtxt(name5)
    N60 = np.genfromtxt(name60)

    plot_contribution(fig, ax1, nameRef, False) # True, cb1
    plot_contribution(fig, ax2, name5, False)
    plot_contribution(fig, ax3, name60, True, cb1) # False

    ax1.text(0.5,1.08,r"$(N+F_{1:4+}) = "+str(int(np.sum(Nmaxref)))+"$", transform = ax1.transAxes, verticalalignment = 'center', horizontalalignment = 'center', color = "dimgrey")
    ax2.text(0.5,1.1,r"$"+str(int(np.sum(N5)))+"$", transform = ax2.transAxes, verticalalignment = 'center', horizontalalignment = 'center', color = "dimgrey")
    ax3.text(0.5,1.1,r"$"+str(int(np.sum(N60)))+"$", transform = ax3.transAxes, verticalalignment = 'center', horizontalalignment = 'center', color = "dimgrey")

    mask = np.ones((30,30))
    plot_local_impact(fig, ax4, name5, Nmaxref, "C2", mask, suffix, cb3)
    # Show not treated pixels
    mask = np.genfromtxt("../outputs_paper/optimization_control/pixel"+str(max_pxl_hetero)+"_duration365_obj2"+suffix+"/space/mask.txt")
    plot_local_impact(fig, ax5, name60, Nmaxref, "C3", mask, suffix, cb2)
    ax5.text(0.5,-0.1,"cells without control", transform = ax5.transAxes, verticalalignment = 'center', horizontalalignment = 'center', color = "cyan")

    plt.savefig("./fig_revised/fig_2_revised"+suffix+".tiff")

if __name__=="__main__":
# Uncomment block for the figure you want to reproduce

#    sizeOfFont = 30
#    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
#    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
#        size=sizeOfFont, weight='bold', stretch='normal')
#    rc('font', **fontProperties)
#    figS6("../outputs_paper/ref_3y/time/pop.txt","../input/environment/temp30/", 3, "all", "mean_IC95")
#    figS6("../outputs_paper/ref_3y_no_mig/time/pop.txt","../input/environment/temp30/", 3, "all", "mean_IC95", "_no_mig")

#    sizeOfFont = 24
#    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
#    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
#        size=sizeOfFont, weight='bold', stretch='normal')
#    rc('font', **fontProperties)
#    figS9()
#    figS9("_no_mig")

    sizeOfFont = 24
    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
        size=sizeOfFont, weight='bold', stretch='normal')
    rc('font', **fontProperties)
    fig2()
#    fig2("_no_mig")

#    sizeOfFont = 19
#    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
#    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
#        size=sizeOfFont, weight='bold', stretch='normal')
#    rc('font', **fontProperties)
#    figS12_control_homo("../input/environment/temp30/")
#    figS12_control_homo("../input/environment/temp30/", "_no_mig")

#    sizeOfFont = 22
#    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
#    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
#        size=sizeOfFont, weight='bold', stretch='normal')
#    rc('font', **fontProperties)
#    figS13_control_hetero("../input/environment/temp30/")
#    figS13_control_hetero("../input/environment/temp30/", "_no_mig")

    sizeOfFont = 19
    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
        size=sizeOfFont, weight='bold', stretch='normal')
    rc('font', **fontProperties)
    fig4_resurgence_hetero("../input/environment/temp30/")
#    fig4_resurgence_hetero("../input/environment/temp30/", "_no_mig")

    sizeOfFont = 19
    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
        size=sizeOfFont, weight='bold', stretch='normal')
    rc('font', **fontProperties)
    fig3_resurgence_homo("../input/environment/temp30/")
#    fig3_resurgence_homo("../input/environment/temp30/", "_no_mig")

#    sizeOfFont = 17
#    fontProperties = {'size' : sizeOfFont} # 'family':'sans-serif','sans-serif':['Arial'], 'weight' : 'normal',
#    ticks_font = font_manager.FontProperties(family='monospace', style='normal',
#        size=sizeOfFont, weight='bold', stretch='normal')
#    rc('font', **fontProperties)
#    plot_K()
