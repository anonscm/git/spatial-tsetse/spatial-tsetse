## ---------------------------
##
## Script name: pupae_development_calibration.R
##
## Purpose of script: Calibrate the rate of development of pupae in a mechanistic model
## Data from CIRDES, 2009, Augustin Bancé
## Function of temperature, sigmoidal curve
##
## Author: Helene Cecilia
##
## Date Created: 2020-09-01

rm(list=ls())

## Loading Packages  ------------------
library(nls2)
library(ggplot2)
library(ggthemes)

## Set Work Directory ------------------------------------------------------
setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) # set to source file location 


# ----- Model selection / revision -----

##
dat.rev <- read.csv("pupae_data_formatted_revision.csv", stringsAsFactors = FALSE, dec=".")
str(dat.rev)

ggplot(dat.rev, aes(temp, duration)) +
  geom_point() +
  geom_smooth(se = TRUE, span = 1)  # se : display confidence interval / span : amount of smoothing

init <- getInitial(duration ~ SSlogis(temp, Asym, xmid, scal), data = dat.rev)
init <- round(init)

model1.rev <- nls(duration ~ 20 + SSlogis(temp, Asym, xmid, scal), trace = TRUE,
              start = list(Asym = init[1], xmid = init[2], scal = init[3]),
              control = nls.control(minFactor = 1 / 2048, tol = 1e-04, maxiter = 300),
              data = dat.rev, weights = n)


model2.rev <- nls(duration ~ SSfpl(temp, A, B, xmid, scal), trace = TRUE,
              control = nls.control(minFactor = 1 / 2048, tol = 1e-04, maxiter = 300),
              data = dat.rev, weights = n)

###
summary(model1.rev)
summary(model2.rev)

###
sqrt(mean((fitted(model1.rev) - dat.rev$duration)^2))
sqrt(mean((fitted(model2.rev) - dat.rev$duration)^2)) # model 2 is retained for our study

temp_pred.rev <- data.frame(temp = seq(min(dat.rev$temp), 30, by = 0.1))

dfpred.rev <- data.frame(temp_pred = temp_pred.rev$temp,
                     pred1 = as.vector(predict(model1.rev, temp_pred.rev)),
                     pred2 = as.vector(predict(model2.rev, temp_pred.rev)))


####
p <- ggplot(dat.rev, aes(temp, duration)) +
  geom_point() +
  geom_line(data = dfpred.rev, aes(x = temp_pred, y = pred1), col = "red") +
  geom_line(data = dfpred.rev, aes(x = temp_pred, y = pred2), col = "green")

print(p)

